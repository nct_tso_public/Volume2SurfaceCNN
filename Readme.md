Volume to Surface Registration Network (V2S-Net)
================================================

In this project, we train a CNN to perform non-rigid volume to (partial) surface registration. Given only the preoperative state of an organ and a rigidly aligned intraoperative surface, the V2S-Net finds a displacement field which maps the former to the latter. Because it is trained on physically correct deformations, it learns to output solutions which abide by plausible biomechanical constraints.
This repository contains code to generate synthetic deformations of random organ-like structures, train the V2S-Net and use it to infer the displacement of real organs.

This is the code for the following paper, which has been provisionally accepted for MICCAI 2020. Please cite if you use this code in your work.
```
Non-Rigid Volume to Surface Registration using a Data-Driven Biomechanical Model.
Micha Pfeiffer, Carina Riediger, Stefan Leger, Jens-Peter Kühn, Danilo Seppelt,
Ralf-Thorsten Hoffmann, Jürgen Weitz and Stefanie Speidel. 2020.
```

<img src="Documentation/InSilicoSuccessCase1967.png" alt="InSilicoCase" width="60%">

Requirements:
-----------------

- python3 (Tested with version 3.5)
- [pytorch](https://pytorch.org/) (Tested with version 1.4.0)
- [Gmsh](https://gmsh.info/) (Tested with version 4.5.6)
- [Blender3D](https://blender.org) (Tested with version 2.76)
- [Elmer](https://github.com/elmercsc/elmerfem) (Tested with version 8.4)

Python modules:

- tensorboardX (Tested with version 1.8)
- vtk (Tested with version 8.1.0)
- matplotlib (Tested with version 3.0.3)

Data Generation:
-----------------

To generate random training data, run:

```bash
cd DataGeneration
python generate.py 100 ../SynData
```

This will try to generate 100 random samples in the "Data" folder. Note that the actual number of generated samples might be lower, since simulations which don't converge are discarded. Samples which failed will not have a "voxelized.vts" file in the folder.

Instead of generating random meshes, you can also generate random simulations using a fixed predefined surface mesh (such as a liver extracted from CT) by adding `--mesh liver_surface.stl`.

Generating the data can take a long time. We recommend running the generate script on multiple computers. By starting it with a different `--startNum` you can control which samples are created on which computer. Once done, simply copy all result folders to a single machine to start training.

See `python generate.py --help` for further options.

Training:
----------------

To train the model on the 100 samples generated above, run:
```bash
cd Model
python train.py --num 100 ../SynData --outputFolder ../Trained
```
The first 95% of the data samples will be used for training, the rest for validation.
Logs, model weights and a copy of the currently used scripts will be saved in ../Trained.
See `python train.py --help` for more command line options.

Inference:
----------------
To apply a trained network to a sample (consisting of a preoperative volume and intraoperative surface), use the `apply.py` script. This script expects the input folder to be structured just like the training data. In the simplest case, this would be:

```
SynTest/
	00000/
		voxelized.vts
```

If you have a preoperative mesh and an intraoperative mesh which are already rigidly aligned, use the `voxelize.py` script to calculate the signed distance fields. Note that inputs should be in Meters (not millimeters!). If that's not the case, use the script's `--scaleInput` flag. Various input formats are supported, checkout `python voxelize.py --help`.

```
cd DataGeneration
python voxelize.py preop.vtk --SDF --center --scaleInput 0.001 --outputGrid ../LiverTest/00000/voxelized.vts

python voxelize.py intraop.stl --DF --reuseTransform --outputGrid ../LiverTest/00000/voxelized.vts
```
The first call to `voxelize.py` calculates the _signed_ distance function for the _preoperative_ volume, after calculating a rigid transform to move center it around (0,0,0). The second call computes the _unsigned_ distance field of the intraoperative data, re-using the same transform to keep the meshes aligned. Note that any applied transform will be stored with the output, so it can be re-used later to move the displacement field back to the original position of the input data (see `deformMesh.py`).

Then use the apply script to let the V2S-Net estimate the displacement:
```
cd Model
python apply.py ../LiverTest/ --modelFolder ../Trained/checkpoints --outputFolder ../LiverTest/Out/ --num 1
```

### Applying displacement field:

The estimated displaceent field can be applyed to 3D meshes (internal vessels, tumors...) by using the `deformMesh.py` script. This will iterate over all points in the 3D mesh, interpolate into the displacement field at their position and add this displacement to their position.Since the network only estimates a displacement inside the preoperative volume, this only works for points which lie _inside_ the organ (i.e. inside the previously used preop.vtk).

### Testing on synthetic data:

You can also generate another test data set with the `generate.py` script (see above) and then apply the net to this data:

```
python apply.py ../SynTest/ --modelFolder ../Trained/checkpoints --outputFolder ../SynTest/Out/ --num 1000
```

This will also analyze the results and save some plots in the same folder.


Pretrained Weights:
----------------

You can download the weights which we used to produce the results in the paper [here](https://caruscloud.uniklinikum-dresden.de/index.php/s/kpEbWMQNgSESFZz). This network was trained with default settings (i.e grid size of 64*64*64 voxels, grid side length of 0.3 meters).

Visualization and Debugging:
----------------

To visualize both the training data and the network results we use [Paraview](https://www.paraview.org/).

### Preoperative Surface:
Load a .vts file, then apply a "Threshold Filter" to only show voxels with a negative value on the `preoperativeSurface` field. This will only show voxels which are "inside" the preoperative volume.

![Threshold Filter](Documentation/ThresholdFilter.png)

### Intraoperative Surface:
Alternatively, you can select only the intraoperative surface by applying a Threshold Filter on the `intraoperativeSurface` field. Note that this time you should use values from 0 to (roughly) your voxel size.

![Threshold Filter Intraoperative](Documentation/ThresholdFilter2.png)

### Displacement:
To see the displacement (simulated or estimated by the V2S-Net), use a "Warp-By-Vector" filter, and choose the displacemet you want to see in the "Vectors" dropdown. Optionally color by the same displacement vector.

![Threshold Filter Intraoperative](Documentation/WarpByVector.png)

