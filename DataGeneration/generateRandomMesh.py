import bpy
import mathutils
import random
import math
import os
import sys
import bmesh
import argparse
import traceback

d = os.path.dirname(bpy.data.filepath)
if not d in sys.path:
    sys.path.append(d)
from utils import *


# Remove all arguments passed to Blender, only take those after the double dash '--' into account:
argv = sys.argv
print("sys.argv",sys.argv)
if "--" in argv:
    argv = argv[argv.index("--") + 1:]
else:
    argv = []

parser = argparse.ArgumentParser(description="Generate a random mesh using Blender functionality. Tested with Blender 2.79.")
parser.add_argument("--seed", type=int, default=None, help="Number to seed the random generator with. If [num] is greater than 1, the ID of the mesh will be used as seed." )
parser.add_argument("--num", type=int, default=1, help="Number of meshes to generate")
parser.add_argument("--startNum", type=int, default=0, help="Optionally start from this simulation number.")
parser.add_argument("--outdir", type=path, default=".", help="Folder in which to save the random mesh.")
parser.add_argument("--bounds", type=float, default=0.3, help="Size of the area in which the object must lie")
args = parser.parse_args(argv)

# Randomly scale a current selection
def randomScale( bm, i, extrusions, verts ):
    # Randomly discard some of the vertices:
    vertCoords = []
    for vert in verts:
        if random.random() > 0.3:
            vertCoords.append( vert.co )
    # Make sure you didn't discard all verts:
    if len(vertCoords) == 0:
        vertCoords = [vert.co for vert in verts]
    
    pivot = sum(vertCoords, mathutils.Vector()) / len(vertCoords)
    
    minScale = (extrusions-i)/extrusions*0.5 + 0.3
    maxScale = minScale + 0.6
    scale = random.uniform( minScale, maxScale )# - abs((float(i)-extrusions/2)/(extrusions/2))*0.4
    #bpy.ops.transform.resize(value=(scale,scale,scale));
    #bmesh.ops.scale(bm, vec=mathutils.Vector((scale,scale,scale)), verts=verts, space=mat)
    for vert in verts:
        vert.co = pivot + (vert.co-pivot)*scale


def getEdges( mesh, vert ):
    edges = []
    for e in mesh.edges:
        if e.vertices[0] == vert.index or e.vertices[1] == vert.index:
            edges.append(e)
    return edges

def otherVert( mesh, edge, vert ):
    if vert.index == edge.vertices[0]:
        return mesh.vertices[edge.vertices[1]]
    return mesh.vertices[edge.vertices[0]]

def select_more( bm, ntimes=1 ):
    for i in range(0, ntimes):
        sel_verts = [v for v in bm.verts if v.select]
        for v in sel_verts:
            for f in v.link_faces:
                f.select_set(True)
    #sel_edges = [ed for ed in bm.edges if ed.select]
    #for ed in sel_edges:
    #   for f in ed.link_faces:
    #       f.select_set(True)

def deleteIslands():
    # split into loose parts
    bpy.ops.mesh.separate(type='LOOSE')
    # object mode
    bpy.ops.object.mode_set(mode='OBJECT')

    parts = bpy.context.selected_objects
    # sort by number of verts (last has most)
    parts.sort(key=lambda o: len(o.data.vertices))
    # print
    for part in parts:
        print(part.name, len(part.data.vertices))
        part.select = True

    parts[-1].select = False
    largest = parts[-1]

    bpy.ops.object.delete()
    
    largest.name = "RandomMesh"
    largest.select = True
    parts = bpy.context.selected_objects
    for part in parts:
        print("still there:", part.name, len(part.data.vertices))

for i in range(args.startNum,args.startNum+args.num):

    try:
        outdir = path( os.path.join( args.outdir, "{:05d}".format(i) ) )
        
        if args.num > 1:
            random.seed( i+1 )
        else:
            random.seed(args.seed)

        # Go to object mode if possible:
        if bpy.context.object != None:
            if bpy.context.object.mode != 'OBJECT':
                bpy.ops.object.mode_set(mode='OBJECT')

        # Clear previous meshes:
        for o in bpy.data.objects:
            o.select = False
            
        for o in bpy.data.objects:
            if "RandomMesh" in o.name:
                o.select = True
        bpy.ops.object.delete()
        
        scene = bpy.context.scene
        
        # Create an empty mesh and the object.
        mesh = bpy.data.meshes.new('RandomMesh')
        object = bpy.data.objects.new("RandomMesh", mesh)

        # Add the object into the scene.
        scene.objects.link(object)
        scene.objects.active = object
        object.select = True

        # Construct the bmesh sphere and assign it to the blender mesh.
        bm = bmesh.new()
        bmesh.ops.create_icosphere(bm, subdivisions=2 , diameter=random.uniform(0.05,0.15))
        bm.to_mesh(mesh)

        #bpy.ops.object.shade_smooth()
        

        #bpy.ops.object.select_all(action='DESELECT')
        #object.select = True
        #bpy.context.scene.objects.active = object
        #bpy.ops.object.mode_set(mode='EDIT')
        #bpy.ops.mesh.select_all(action = 'DESELECT')

        #bpy.ops.object.mode_set(mode = 'OBJECT')
        #object.data.polygons[5].select = True
        #bpy.ops.object.mode_set(mode = 'EDIT')

        # Extrude the mesh a random number of times:
        extrusions = random.randint(1,4)
        bm.faces.ensure_lookup_table()
        
        bpy.ops.object.mode_set(mode='EDIT')
        for i in range( 0, extrusions ):
            
            for v in bm.verts:
                v.select = False
            bm.select_flush(False)
            bm.faces.ensure_lookup_table()
            id = random.randint(0, len(bm.faces)-1)
            face = bm.faces[id]
            face.select_set(True)
            
            select_more(bm, ntimes=random.randint(1,2))
            
            # Randomly scale the selected face:
            #randomScale( bm, i, extrusions, face.verts )
            
            #bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0.05), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
            #bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0.05,0,0)})
            selection = [f for f in bm.faces if f.select]
            extruded = bmesh.ops.extrude_face_region(bm, geom=selection)
            vec = -selection[0].normal*0.05
            bmesh.ops.delete(bm, geom=selection, context=5) # Delete previous faces
            verts = [v for v in extruded['geom'] if isinstance(v, bmesh.types.BMVert)]
            faces = [f for f in extruded['geom'] if isinstance(f, bmesh.types.BMFace)]
            #vec = mathutils.Vector((0.05, 0, 0))
            bmesh.ops.translate(bm, vec=vec, verts=verts)
            

        bpy.ops.object.mode_set(mode='OBJECT')
        bm.to_mesh(object.data)
        bm.free()
        
        
        # Randomly rotate the object:
        bpy.ops.object.mode_set(mode='OBJECT')
        eul = mathutils.Euler((random.uniform(0,2*math.pi),random.uniform(0,2*math.pi),random.uniform(0,2*math.pi)), 'XYZ')
        object.rotation_mode = "QUATERNION"
        object.rotation_quaternion = eul.to_quaternion()
        bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
        object.rotation_euler = eul
        
        # Duplicate object:
        me_copy = object.data.copy()
        duplicated = bpy.data.objects.new("RandomMeshCopy", me_copy)
        vec = mathutils.Vector((0,0,0))
        while vec.length < 0.05:
            x = random.uniform(-0.15,0.15 )
            y = random.uniform(-0.15,0.15 )
            z = random.uniform(-0.15,0.15 )
            vec = mathutils.Vector((x,y,z))
        duplicated.location = vec
        x=random.uniform(-math.pi,math.pi)
        y=random.uniform(-math.pi,math.pi)
        z=random.uniform(-math.pi,math.pi)
        duplicated.rotation_euler = (x,y,z)
        scene.objects.link(duplicated)
        scene.update()
        
        # Subtract the duplicate from this object:
        boolModifier = object.modifiers.new(type="BOOLEAN", name="bool")
        boolModifier.object = duplicated
        boolModifier.operation = "DIFFERENCE"
        bpy.ops.object.modifier_apply(apply_as="DATA",modifier="bool")
        
        # Scale object down if necessary:
        for dim in object.dimensions:
            if dim > args.bounds:
                print("Object is greater than maximum bounds ({:f}, current size {:f}). Scaling down.".format(args.bounds, dim))
                scale = 0.9*args.bounds/dim
                bpy.context.scene.objects.active.scale = (scale,scale,scale)
                #raise ValueError("Object is greater than maximum bounds ({:f})".format(args.bounds))
                
        # Move the object (and the curve) to fit into a predefined bounding box:
        local_bbox_center = 0.125 * sum((mathutils.Vector(b) for b in object.bound_box), mathutils.Vector())
        origOffset = -local_bbox_center     # Offset to move object into world origin
        object.location = origOffset

        # Offset to randomly place object inside bounding box:
        maxOffset = (mathutils.Vector((args.bounds, args.bounds, args.bounds)) - object.dimensions)*0.5;
        randOffset = mathutils.Vector(( random.uniform(-maxOffset.x,maxOffset.x),
                            random.uniform(-maxOffset.y,maxOffset.y),
                            random.uniform(-maxOffset.z,maxOffset.z) ))
        object.location += randOffset

            
        bpy.ops.object.mode_set(mode='OBJECT')
        remesh = object.modifiers.new(type="REMESH", name="remesh")
        remesh.octree_depth = 3
        remesh.sharpness = 0.1
        bpy.ops.object.modifier_apply(apply_as="DATA",modifier="remesh")
        
        
        object.modifiers.new(type="SUBSURF", name="subdivision")
        bpy.ops.object.mode_set(mode='EDIT')
        # Randomly add creased edges:
        for k in range(0,2):
            if random.random() > 0.2:
                edgeLen = random.randint( 3, 10 )
                crease = random.uniform(0.7,0.9)
                # Start with a random vertex:
                vert = mesh.vertices[random.randint(0,len(mesh.vertices)-1)]
                for i in range(0,edgeLen):
                    edges = getEdges( mesh, vert )
                    edge = edges[random.randint(0,len(edges)-1)]
                    edge.crease = crease
                    vert = otherVert( mesh, edge, vert )
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.modifier_apply(apply_as="DATA",modifier="subdivision")

        # Clean up possible non-manifolds
        cleanupAttempts = 0
        isClean = False
        ob = bpy.context.object # get the active object for example
        mesh = ob.data
        while not isClean and cleanupAttempts < 15:
            isClean = True
            cleanupAttempts = cleanupAttempts + 1

            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(type="VERT")
            bpy.ops.mesh.select_non_manifold()

            # Workaround to ensure the v.select flag is set correctly on the vertices:
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.ops.object.mode_set(mode='EDIT')

            nonManifoldVerts = [v for v in mesh.vertices if v.select]
            if len(nonManifoldVerts) > 0:
                print("Cleaning non-manifold verts: ", len(nonManifoldVerts))
                isClean = False
                bpy.ops.mesh.dissolve_verts()
                bpy.ops.mesh.vert_connect_concave()

        deleteIslands()

        try:
            bpy.ops.object.mode_set(mode='OBJECT')
        except:
            pass
        
        # Export:
        filepath = os.path.join(outdir,str('surface.stl'))
        bpy.ops.export_mesh.stl(filepath=filepath, check_existing=False, filter_glob="*.stl", ascii=True, use_mesh_modifiers=True, axis_forward='Y', axis_up='Z', global_scale=1.0)
        print("Generated random mesh. Exported to " + filepath);

        for o in bpy.data.objects:
            o.select = False
        for o in bpy.data.objects:
            if "RandomMeshCopy" in o.name:
                o.select = True
            
        bpy.ops.object.delete()
            
    except:
        print( "Could not generate model. Seed:", i )
        print( "Unexpected error:", sys.exc_info()[0] )
        print( traceback.format_exc() )

#bpy.ops.wm.quit_blender()
