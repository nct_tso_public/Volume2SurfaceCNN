import argparse
import voxelize
import voxelizeDisplacementField
from utils import *
import os

parser = argparse.ArgumentParser(description="Given an initial volume mesh, a partial surface and a ground truth deformation, create a testing scenario.")
parser.add_argument("preop_volume", type=filepath, help="Initial volume mesh")
parser.add_argument("intraop_surface", type=filepath, help="Intraoperative (partial) surface")
parser.add_argument("intraop_volume", type=filepath, help="Ground truth deformation of preop_volume")
parser.add_argument("--num", type=int, default=10, help="How many samples to create")
parser.add_argument("--max_random_displacement", type=float, default=1e-2, help="Maximum random displacement to apply to intraop_surface and intraop_volume (in meters) for each sample")
parser.add_argument("--max_random_angle", type=float, default=4, help="Maximum random angle to apply to intraop_surface and intraop_volume (in degrees) for each sample")
parser.add_argument("--size", type=float, default=0.3, help="Size of resulting grid")
parser.add_argument("--grid_size", type=int, default=64, help="Number of voxels per dimension")
parser.add_argument("outdir", type=path, default=".", help="Folder in which to save the simulation files.")

args = parser.parse_args()

def loadInputFile( filename ):
    # Load the input mesh:
    fileType = filename[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName( filename )
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".obj":
        reader = vtkOBJReader()
        reader.SetFileName( filename )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        mesh = reader.GetOutput()
    return mesh


# Create the pre-oprative signed distance field:
print("Loading preoperative volume")
preop_volume = loadInputFile( args.preop_volume )
if preop_volume.GetNumberOfPoints() <= 0:    # Loaded successfully?
    raise IOError("Could not read {}".format(args.preop_volume))
preop_volume = unstructuredGridToPolyData( preop_volume )
preop_surface = extractSurface( preop_volume )

print("Voxelizing preoperative volume")
grid = voxelize.createGrid( args.size, args.grid_size )
voxelize.distanceField( preop_surface, grid, "preoperativeSurface", signed=True )

# Load the original preoperative meshes:
print("Loading intraoperative meshes")
intraop_surface = loadInputFile( args.intraop_surface )
intraop_volume = loadInputFile( args.intraop_volume )

for i in range( 0, args.num ):
    print( "Generating case:", i )
    curDir = directoryForIndex( i, args.outdir )
    curGrid = vtkStructuredGrid()
    curGrid.DeepCopy( grid )

    x = (random.random()*2-1)*args.max_random_displacement
    y = (random.random()*2-1)*args.max_random_displacement
    z = (random.random()*2-1)*args.max_random_displacement
    tf = vtkTransform()
    tf.Translate( x, y, z )
    v = vtkVector3d( random.random()-0.5, random.random()-0.5, random.random()-0.5 )
    if v.Norm() == 0:
        v = vtkVector3d( 1,0,0)
    v.Normalize()
    tf.RotateWXYZ( (random.random()*2-1)*args.max_random_angle, v.GetX(), v.GetY(), v.GetZ() )

    print( "Random transform:", tf.GetMatrix() )

    tfFilter = vtkTransformFilter()
    tfFilter.SetTransform( tf )
    tfFilter.SetInputData( intraop_surface )
    tfFilter.Update()
    intraop_surface_rnd = tfFilter.GetOutput()

    tfFilter.SetInputData( intraop_volume )
    tfFilter.Update()
    intraop_volume_rnd = tfFilter.GetOutput()

    # Voxelize intraoperative surface data:
    voxelize.distanceField( intraop_surface_rnd, curGrid, "intraoperativeSurface", signed=False )
    # Voxelize ground truth displacement:
    displacement_mesh = voxelizeDisplacementField.calcDisplacement( preop_volume, intraop_volume_rnd )
    curGrid = voxelizeDisplacementField.interpolateToGrid( displacement_mesh, curGrid, args.size/(args.grid_size-1) )

    
    writer = vtkXMLPolyDataWriter()
    writer.SetFileName( os.path.join( curDir, "intraop_surface.vtp" ) )
    writer.SetInputData( intraop_surface_rnd )
    writer.Update()

    intraop_volume_rnd = unstructuredGridToPolyData( intraop_volume_rnd )
    writer = vtkXMLPolyDataWriter()
    writer.SetFileName( os.path.join( curDir, "intraop_deformed_volume.vtp" ) )
    writer.SetInputData( intraop_volume_rnd )
    writer.Update()

    print("Writing to {}".format( os.path.join(curDir,"voxelized.vts") ))
    writer = vtkXMLStructuredGridWriter()
    writer.SetFileName( os.path.join(curDir,"voxelized.vts" ) )
    writer.SetInputData( curGrid )
    writer.Update()


