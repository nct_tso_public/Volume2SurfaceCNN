import sys
from utils import *
from vtk import *
import math
import inspect
import pcl

#def distanceField( surfaceMesh, targetGrid, targetArrayName, signed=True ):
#
#    geometryFilter = vtkGeometryFilter()
#    geometryFilter.SetInputData( targetGrid )
#    geometryFilter.Update()
#
#    distanceFilter = vtkDistancePolyDataFilter()
#    distanceFilter.SetInputData( 0, geometryFilter.GetOutput() )
#    distanceFilter.SetInputData( 1, surfaceMesh )
#    if not signed:
#        distanceFilter.SignedDistanceOff()
#    distanceFilter.Update()
#
#    array = distanceFilter.GetOutput().GetPointData().GetArray("Distance")
#    array.SetName( targetArrayName )
#    targetGrid.GetPointData().AddArray( array )

def checkInside( p, surfPoint, n ):
    pInside = [0]*3
    pOutside = [0]*3
    pOutside[0] = surfPoint[0] + n[0]
    pOutside[1] = surfPoint[1] + n[1]
    pOutside[2] = surfPoint[2] + n[2]
    pInside[0] = surfPoint[0] - n[0]
    pInside[1] = surfPoint[1] - n[1]
    pInside[2] = surfPoint[2] - n[2]
    dist2PointOutside = vtkMath.Distance2BetweenPoints( p, pOutside )
    dist2PointInside = vtkMath.Distance2BetweenPoints( p, pInside )
    return dist2PointInside <= dist2PointOutside


def distanceField( surfaceMesh, targetGrid, targetArrayName, signed=False ):
    #if signed:
    #    print(surfaceMesh)

    # Estimate normals for the surface. Will be needed for determining inside/outside:
    #if signed:
    #    normalGenerator = vtkPolyDataNormals()
    #    normalGenerator.SetInputData( surfaceMesh )
    #    normalGenerator.ComputePointNormalsOn()
    #    normalGenerator.ComputeCellNormalsOff()
    #    normalGenerator.AutoOrientNormalsOn()
    #    normalGenerator.SplittingOff()
    #    normalGenerator.Update()
    #    pointNormals = normalGenerator.GetOutput().GetPointData().GetArray("Normals")

    # Initialize distance field:
    df = vtkDoubleArray()
    df.SetNumberOfTuples( targetGrid.GetNumberOfPoints() )
    df.SetName(targetArrayName)

    # Data structure to quickly find points:
    #pointLocator = vtkPointLocator()
    #pointLocator.SetDataSet( surfaceMesh )
    #pointLocator.BuildLocator()

    # Data structure to quickly find cells:
    cellLocator = vtkCellLocator()
    cellLocator.SetDataSet( surfaceMesh )
    cellLocator.BuildLocator()


    for i in range(0, targetGrid.GetNumberOfPoints() ):
        # Take a point from the target...
        testPoint = [0]*3
        targetGrid.GetPoint( i, testPoint )
        # ... find the point in the surface closest to it
        cID, subID, dist2 = mutable(0), mutable(0), mutable(0.0)
        closestPoint = [0]*3
        
        cellLocator.FindClosestPoint( testPoint, closestPoint, cID, subID, dist2 )
        #closestPoint = [0]*3
        #surfaceMesh.GetPoint( closestPointID, closestPoint )
        #dist = math.sqrt( vtkMath.Distance2BetweenPoints( testPoint, closestPoint ) )
        dist = math.sqrt(dist2)
        #if signed:
        #    closestVertexID = pointLocator.FindClosestPoint( testPoint )
        #    closestVertex = [0]*3
        #    surfaceMesh.GetPoint( closestVertexID, closestVertex )
        #    normal = [0]*3
        #    pointNormals.GetTuple( closestVertexID, normal )
        #    if checkInside( testPoint, closestVertex, normal ):
        #        dist = -dist

        df.SetTuple1( i, dist )

    if signed:
        pts = vtkPolyData()
        pts.SetPoints( targetGrid.GetPoints() )

        enclosedPointSelector = vtkSelectEnclosedPoints()
        enclosedPointSelector.CheckSurfaceOn()
        enclosedPointSelector.SetInputData( pts )
        enclosedPointSelector.SetSurfaceData( surfaceMesh )
        enclosedPointSelector.SetTolerance( 1e-9 )
        enclosedPointSelector.Update()
        enclosedPoints = enclosedPointSelector.GetOutput()

        for i in range(0, targetGrid.GetNumberOfPoints() ):
            if enclosedPointSelector.IsInside(i):
                df.SetTuple1( i, -df.GetTuple1(i) )     # invert sign

    targetGrid.GetPointData().AddArray( df )

def distanceFieldFromCloud( surfaceCloud, targetGrid, targetArrayName ):

    # Initialize distance field:
    df = vtkDoubleArray()
    df.SetNumberOfTuples( targetGrid.GetNumberOfPoints() )
    df.SetName(targetArrayName)

    # Data structure to quickly find cells:
    pointLocator = vtkPointLocator()
    pointLocator.SetDataSet( surfaceCloud )
    pointLocator.BuildLocator()

    for i in range(0, targetGrid.GetNumberOfPoints() ):
        # Take a point from the target...
        testPoint = [0]*3
        targetGrid.GetPoint( i, testPoint )
        # ... find the point in the surface closest to it
        cID, subID, dist2 = mutable(0), mutable(0), mutable(0.0)
        closestPoint = [0]*3
        
        closestPointID = pointLocator.FindClosestPoint( testPoint )
        closestPoint = [0]*3
        surfaceCloud.GetPoint( closestPointID, closestPoint )
        #closestPoint = [0]*3
        #surfaceMesh.GetPoint( closestPointID, closestPoint )
        dist = math.sqrt( vtkMath.Distance2BetweenPoints( testPoint, closestPoint ) )
        #dist = math.sqrt(dist2)
        df.SetTuple1( i, dist )

    targetGrid.GetPointData().AddArray( df )


def createGrid( size, gridSize, center=[0,0,0] ):
    grid = vtkStructuredGrid()
    grid.SetDimensions( (gridSize,gridSize,gridSize) )
    points = vtkPoints()
    points.SetNumberOfPoints( gridSize**3 )
    pID = 0
    startX = -size/2 + center[0]
    startY = -size/2 + center[1]
    startZ = -size/2 + center[2]
    d = size/(gridSize-1)
    for i in range( 0, gridSize ):
        for j in range( 0, gridSize ):
            for k in range( 0, gridSize ):
                x = startX + d*k
                y = startY + d*j
                z = startZ + d*i
                points.SetPoint( pID, x, y, z )
                pID += 1
    grid.SetPoints( points )
    return grid

def storeTransformationMatrix( grid, tf ):
    mat = tf.GetMatrix()
    matArray = vtkDoubleArray()
    matArray.SetNumberOfTuples(16)
    matArray.SetNumberOfComponents(1)
    matArray.SetName( "TransformationMatrix" )
    for row in range(0,4):
        for col in range(0,4):
            matArray.SetTuple1( row*4+col, mat.GetElement( row, col ) )
    grid.GetFieldData().AddArray(matArray)

def loadTransformationMatrix( grid ):
    matArray = grid.GetFieldData().GetArray( "TransformationMatrix" )
    if matArray:
        tf = vtkTransform()
        mat = vtkMatrix4x4()
        for i in range( 0, 16 ):
            val = matArray.GetTuple1( i )
            mat.SetElement( int(i/4), i % 4, val )
        tf.SetMatrix( mat )
        return tf
    else:
        raise IOError("No 'TransformationMatrix' array found in field data.")


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Discretize a given irregular mesh or a point cloud onto a regular grid. The result will be a regular grid filled with the (signed) distance field, representing the original mesh. Note: This program only looks at the surface triangles in the input mesh and ignores any volume elements. Use --SDF and --DF to control whether the signed distance field or the distance field should be calculated. Supported file types: stl, obj, vtk, vtu, vtp, pcd. When a point cloud is given, only --DF is supported.")
    parser.add_argument("mesh", type=filepath, help="Surface to voxelize. If --SDF is set, this must be a closed manifold! For --DF it can be a partial surface mesh of point cloud.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--SDF", action="store_true", help="Calculate the signed distance function. Mesh must contain a closed surface of triangles!")
    group.add_argument("--DF", action="store_true", help="Calculate the distance function. Mesh can be a partial surface or point cloud.")
    parser.add_argument("--arrayName", type=str, default="", help="Name of the generated array. Defaults to \"signedDistance\" or \"distance\" depending on whether --DF or --SDF is given.")
    parser.add_argument("--outputGrid", default="voxelized.vts", help="Name of the output file to generate. If this file exists, grid size and dimensions are used from the grid and --size and --grid_size are ignored. In this case, the new array is added to the existing file.")
    parser.add_argument("--size", type=float, default=0.3, help="Size of resulting grid (ignored if --outputGrid points to an already existing grid)")
    parser.add_argument("--grid_size", type=int, default=64, help="Number of voxels per dimension (ignored if --outputGrid points to an already existing grid)")
    transformGroup = parser.add_argument_group("Transformation")
    movementGroup = transformGroup.add_mutually_exclusive_group(required=False)
    movementGroup.add_argument("--moveInput", type=float, nargs=3, help="Move the input before transforming to distance field (movement is applied before scaling!)")
    movementGroup.add_argument("--center", action="store_true", help="Center the data around the origin.")
    transformGroup.add_argument("--scaleInput", type=float, help="Scale the input before transforming to distance field (movement is applied before scaling!)")
    transformGroup.add_argument("--reuseTransform", action="store_true", help="Reuse transformation already stored in the grid. Use this if you want to center mesh 1 and then apply the same transformation to mesh 2. Mutually exclusive with --center, --scaleInput and --moveInput.")

    args = parser.parse_args()

    # If no array name was given, use sensible defaults:
    if args.arrayName == "":
        if args.SDF:
            args.arrayName = "preoperativeSurface"
        else:
            args.arrayName = "intraoperativeSurface"

    if not args.outputGrid.endswith(".vts"):
        raise IOError("Output grid needs to be .vts!")

    if args.reuseTransform and (args.center or args.moveInput or args.scaleInput):
        raise IOError("--reuseTransform may not be used together with --center, --moveInput or --scaleInput!")

    inputIsPointCloud = False

    # Load the input mesh:
    fileType = args.mesh[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName( args.mesh )
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".obj":
        reader = vtkOBJReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtp":
        reader = vtkXMLPolyDataReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".ply":
        reader = vtkPLYReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".pcd":
        if args.SDF:
            raise IOError("Input file is .pcd, cannot compute signed distance function (use --DF instead)")

        pc = pcl.load( args.mesh )
        pts = vtkPoints()
        verts = vtkCellArray()
        for i in range( pc.size ):
            pts.InsertNextPoint( pc[i][0], pc[i][1], pc[i][2] )
            verts.InsertNextCell( 1, (i,) )
        mesh = vtkPolyData()
        mesh.SetPoints( pts )
        mesh.SetVerts( verts )

        inputIsPointCloud = True
 
    elif fileType == ".txt":
        if args.SDF:
            raise IOError("Input file is .txt, cannot compute signed distance function (use --DF instead)")

        with open( args.mesh ) as f:
            pts = vtkPoints()
            verts = vtkCellArray()
            i = 0
            for l in f:
                l = l.replace(',', " ")
                x,y,z = l.split(" ", 3)
                x = float(x)
                y = float(y)
                z = float(z)
                pts.InsertNextPoint( x, y, z )
                verts.InsertNextCell( 1, (i,) )
                i = i+1
            mesh = vtkPolyData()
            mesh.SetPoints( pts )
            mesh.SetVerts( verts )

        inputIsPointCloud = True

    else:
        raise IOError("Mesh should be .vtk, .vtu, .vtp, .obj, .stl, .txt, .ply or .pcd file!")


    if not isinstance( mesh, vtkPolyData ):
        print("Mesh is of type ", type(mesh))
        print("Converting to poly data:")
        mesh = unstructuredGridToPolyData( mesh )

    bounds = [0]*6;
    mesh.GetBounds(bounds)
    print("Resulting bounds: ({:.3f}-{:.3f}, {:.3f}-{:.3f}, {:.3f}-{:.3f})".format(*bounds))
    #cleaner = vtkCleanPolyData()
    #cleaner.SetInputData( mesh )
    #cleaner.SetTolerance( 1e-3 )
    #cleaner.ToleranceIsAbsoluteOn()
    #cleaner.PointMergingOn()
    #cleaner.ConvertStripsToPolysOff()
    #cleaner.ConvertPolysToLinesOff()
    #cleaner.ConvertLinesToPointsOff()
    #cleaner.Update()
    #mesh = cleaner.GetOutput()

    ####################################################
    # Load the output mesh:
    if os.path.exists( args.outputGrid ):
        reader = vtkXMLStructuredGridReader()
        reader.SetFileName( args.outputGrid )
        reader.Update() 
        grid = reader.GetOutput()
        if grid.GetPointData().GetArray( args.arrayName ):
            err = "The output file {} already has a field named {}!".format(args.outputGrid,args.arrayName)
            raise IOError(err)
        #args.size = grid.GetBounds()
        b = grid.GetBounds()
        args.size = b[1]-b[0]
        args.grid_size = grid.GetDimensions()[0]
    else:
        grid = createGrid( args.size, args.grid_size )


    ####################################################
    ## Transform input mesh:
    tf = vtkTransform()
    if args.scaleInput is not None:
        print("Scaling point cloud by:", args.scaleInput)
        tf.Scale( [args.scaleInput]*3 )
    if args.moveInput is not None:
        print("Moving point cloud by:", args.moveInput)
        tf.Translate( *args.moveInput )
    if args.center:
        bounds = [0]*6;
        mesh.GetBounds(bounds)
        dx = -(bounds[1]+bounds[0])*0.5
        dy = -(bounds[3]+bounds[2])*0.5
        dz = -(bounds[5]+bounds[4])*0.5
        print("Moving point cloud by:", (dx,dy,dz) )
        tf.Translate( (dx,dy,dz) )
    if args.reuseTransform:
        try:
            tf = loadTransformationMatrix( grid )
        except:
            print("Warning: --reuseTransform was set, but no previous transformation found in grid. Won't apply any transformation.")

    tfFilter = vtkTransformFilter()
    tfFilter.SetTransform( tf )
    tfFilter.SetInputData( mesh )
    tfFilter.Update()
    mesh = tfFilter.GetOutput()
    print("Applied transformation before voxelization:", tf.GetMatrix())

    if mesh.GetNumberOfCells() == 0:
        print("No cells detected. Assuming mesh is point cloud.")
        inputIsPointCloud = True
        if args.SDF:
            raise IOError("Input file contains no cells cannot compute signed distance function (use --DF instead)")

    ####################################################
    ## Compute the (signed) distance field on the output grid:
    print("Will save results in array '" + args.arrayName + "'.")
    print("Voxelization")
    if not inputIsPointCloud:
        surface = extractSurface( mesh )
        if args.SDF:
            distanceField( surface, grid, args.arrayName, signed=True )
        else:
            distanceField( surface, grid, args.arrayName, signed=False )
    else:
        distanceFieldFromCloud( mesh, grid, args.arrayName )

    ####################################################
    # Write the applied transform into a field data array:
    storeTransformationMatrix( grid, tf )

    ####################################################
    # Write the applied transform into a field data array:
    outputFolder = os.path.dirname( args.outputGrid )
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)

    print("Writing to {}".format( args.outputGrid ))
    writer = vtkXMLStructuredGridWriter()
    writer.SetFileName( args.outputGrid )
    writer.SetInputData( grid )
    writer.Update()



