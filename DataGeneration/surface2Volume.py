import os
import subprocess
from utils import *
import sys

# Sets up everything for gmsh, then uses gmsh to create a volume mesh from the given surface mesh.
# surfaceFilename should be the path to an .stl file.
def surface2Volume( surfaceFilename, volumeFilename="volume.vtk" ):
    
    dirname = os.path.dirname( surfaceFilename )
    dirname = os.path.join( os.getcwd(), dirname )   # make absolute
    fname = os.path.basename( surfaceFilename )

    with open( os.path.join(dirname, "surface.geo"), "w" ) as f:
        f.write("Merge \"" + fname + "\";\n")
        f.write("Mesh.Algorithm3D = 1;\n")
        f.write("Mesh.Optimize = 1;\n")
        f.write("Mesh.OptimizeNetgen = 1;\n")
        f.write("Mesh.CharacteristicLengthMax = 0.01;\n")
        f.write("Surface Loop(1) = {1};\n")
        f.write("Volume(1) = {1};\n")

    #gmsh surface.geo -o input.vtk -3
    a = ["gmsh", "surface.geo","-o", volumeFilename, "-3"]
    p = subprocess.call( a, cwd=dirname, shell=False, timeout=30 )

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Takes a surface .stl file and fills it with volume elements, turning it into a volume mesh. Requires gmsh to be in your $PATH.")
    parser.add_argument("mesh", type=filepath, help="Surface mesh. Will be filled with volume elements for the simulation.")
    args = parser.parse_args()

    surface2volume( args.mesh )


