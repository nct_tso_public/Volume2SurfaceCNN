import vtk
import os
import csv
from utils import *
import json

# Takes a field data and dumps it into a separate file for faster reading:
def appendFieldDataToInfoList( infoList, sampleID, fieldData ):
    #if sampleID in infoList:
    #    return
    info = {} 
    for i in range( fieldData.GetNumberOfArrays() ):
        arr = fieldData.GetArray(i)
        info[arr.GetName()] = arr.GetTuple1(0)

    infoList[sampleID] = info

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Load voxelized data and extract the field data into separate file.")
    parser.add_argument("dir", type=path, default=".", help="Folder in which to look for voxelized files.")
    parser.add_argument("--num", type=int, default=100, help="How many directories to process.")
    parser.add_argument("--startNum", type=int, default=0, help="First directory to process.")

    args = parser.parse_args()

    infoListFile = os.path.join( args.dir, "info.json" )

    if os.path.exists( infoListFile ):
        infoList = json.load( open( infoListFile ) )
    else:
        infoList = {}

    # Serialize data into file:

    table = vtkTable()

    for i in range( args.startNum, args.startNum + args.num ):
        sampleID = "{:05d}".format(i)
        filename = os.path.join( args.dir, sampleID, "voxelized.vts" )
        if os.path.exists( filename ):
            print(filename)
            reader = vtk.vtkXMLStructuredGridReader()
            reader.SetFileName( filename )
            reader.Update()
            grid = reader.GetOutput()
            appendFieldDataToInfoList( infoList, sampleID, grid.GetFieldData() )

    json.dump( infoList, open( infoListFile, 'w' ), indent=4, sort_keys=True )
