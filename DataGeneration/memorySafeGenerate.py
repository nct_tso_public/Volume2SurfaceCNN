import subprocess

start = 0
end = 10000

step = 10

while start < end:
  #python3 generate.py --startNum 13200 6 /local_home/pfeiffemi/V2SDataLatticeMeshes --skip_mesh_generation
  a = ["python3"]
  a += ["generate.py"]
  a += ["--startNum"]
  a += [str(start)]
  a += [str(step)]
  a += ["/local_home/pfeiffemi/V2SDataSmallerSurface"]
  #a += ["--skip_mesh_generation"]
  print(a)
  p = subprocess.Popen(a)
  p.wait()
  start += step
