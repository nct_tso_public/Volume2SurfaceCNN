import vtk
import random
from utils import *
import math

def makePartialSurface( mesh, seed, maxTranslation=0.02, maxRotation=5, path="" ):

    random.seed( seed )

    # Extract a random surface region:
    triangleIDs = []
    for i in range(0,mesh.GetNumberOfCells()):
        if mesh.GetCell(i).GetCellType() == VTK_TRIANGLE:
            triangleIDs.append(i)

    b = mesh.GetBounds()
    sides = (b[1]-b[0], b[3]-b[2], b[5]-b[4])
    #diag = math.sqrt( sides[0]**2 + sides[1]**2 + sides[2]**2 )
    radius = sides[random.randint(0,2)]*0.5
    sphereSurfaceArea = 4*math.pi*radius**2
    maxRadius = math.sqrt(sphereSurfaceArea/math.pi)
    minRadius = 0.2*maxRadius
    radius = random.uniform(minRadius,maxRadius)
    regionIDs = getRandomSurfaceArea( mesh, radius, triangleIDs, 3 )

    # Fill an array with zeros where we want to cut triangles away and one for those triangles we
    # want to keep. We will later use this for thresholding.
    arr = vtkFloatArray()
    arr.SetNumberOfComponents(1)
    arr.SetNumberOfTuples( mesh.GetNumberOfCells() )
    arr.Fill(0)
    arr.SetName("region")
    for i in range(0, mesh.GetNumberOfCells() ):
        if i in regionIDs:
            arr.SetTuple1(i,1)

    # Optionally remove additional random (smaller) parts of the surface:
    if random.random() > 0.2:
        numCutouts = random.randint(1,2)
        print("Cutting out {:d} random areas".format(numCutouts))
        for j in range(numCutouts):
            # Center the random area around a cell that's already in the selected surface:
            centerTriangleID = regionIDs[random.randint(0,len(regionIDs)-1)]
            cutoutRegionIDs = getRandomSurfaceArea( mesh, random.uniform(radius*0.1,radius*0.5), triangleIDs, 0,
                    centerTriangleID=centerTriangleID )
            for i in range(0, mesh.GetNumberOfCells() ):
                if i in cutoutRegionIDs:
                    arr.SetTuple1(i,-1)      # Set these cells back to "zero" (i.e. "to be removed")

    mesh.GetCellData().AddArray(arr)

    # Debug output:
    #writer = vtkXMLPolyDataWriter()
    #writer.SetInputData( mesh )
    #surfaceFile = os.path.join( path, "surfaceDebug.vtp" )
    #print("Writing to {}".format( surfaceFile ))
    #writer.SetFileName( surfaceFile )
    #writer.Update()


    # Extract surface (as vtkPolyData)
    #geometryFilter = vtkGeometryFilter()
    #geometryFilter.SetInputData( mesh )
    #geometryFilter.Update()

    # Filter out only those triangles/points where "region" is 1 (or more specifically >= 0.5 and <= 2 )
    thresh = vtkThreshold()
    thresh.SetInputData( mesh )
    thresh.SetInputArrayToProcess( 0,0,0, vtkDataObject.FIELD_ASSOCIATION_CELLS, "region" )
    thresh.ThresholdBetween( 0.5, 2.0 )

    #writer = vtkUnstructuredGridWriter()
    #writer.SetFileName( "thresh.vtk" )
    #writer.SetInputConnection( thresh.GetOutputPort() )
    #writer.Update() # Needed because of GetScalarRange

    # Extract surface (as vtkPolyData)
    geometryFilter = vtkGeometryFilter()
    geometryFilter.SetInputConnection( thresh.GetOutputPort() )
    geometryFilter.Update()

    # Randomly rotate and translate the surface
    transformFilter = vtkTransformPolyDataFilter()
    transform = vtkTransform()
    dx = (random.random() - 0.5)*maxTranslation*2
    dy = (random.random() - 0.5)*maxTranslation*2
    dz = (random.random() - 0.5)*maxTranslation*2
    rx = (random.random() - 0.5)*maxRotation*2
    ry = (random.random() - 0.5)*maxRotation*2
    transform.Translate( dx, dy, dz )
    transform.RotateY( ry )
    transform.RotateX( rx )
    transformFilter.SetTransform( transform )
    transformFilter.SetInputData( geometryFilter.GetOutput() )
    transformFilter.Update()

    return transformFilter.GetOutput(), transform

# Apply noise to the points in mesh. Optionally add more points and delete regions
def applyNoise( mesh, seed, maxNoise=1e-2, voxelSize=1e-3 ):
    random.seed( seed )

    print( "Input", mesh.GetNumberOfPoints() )

    # Resample input data:
    sampler = vtkPolyDataPointSampler()
    sampler.SetInputData( mesh )
    sampler.SetDistance( voxelSize*2 );
    sampler.Update();
    mesh = sampler.GetOutput()

    print( "Sampled", mesh.GetNumberOfPoints() )

    points = vtkPoints()
    for i in range( mesh.GetNumberOfPoints() ):
        p = mesh.GetPoint(i)
        if random.random() > 0.3:
            pRandX = p[0]+maxNoise*(random.random()*2-1)
            pRandY = p[1]+maxNoise*(random.random()*2-1)
            pRandZ = p[2]+maxNoise*(random.random()*2-1)
            pNew = (pRandX, pRandY, pRandZ)
            points.InsertNextPoint( pNew )
            #points.InsertNextPoint( p )

    pd = vtkPolyData()
    pd.SetPoints( points )

    return pd

def applyRigidTransform( mesh, transform ):

    rigidDisplacement = vtkDoubleArray()
    rigidDisplacement.SetName( "rigidDisplacement" )
    rigidDisplacement.SetNumberOfComponents( 3 )
    rigidDisplacement.SetNumberOfTuples( mesh.GetNumberOfPoints() )

    # Extract surface (as vtkPolyData)
    geometryFilter = vtkGeometryFilter()
    geometryFilter.SetInputData( mesh )
    geometryFilter.Update()

    transformFilter = vtkTransformPolyDataFilter()
    transformFilter.SetTransform( transform )
    transformFilter.SetInputData( geometryFilter.GetOutput() )
    transformFilter.Update()

    transformed = transformFilter.GetOutput()
    #resultArray = mesh.GetPointData().GetArray( arrayName )
    for i in range(0, mesh.GetNumberOfPoints() ):
        p1 = mesh.GetPoint(i)
        p2 = transformed.GetPoint(i)
        d = [0]*3
        d[0] = p2[0] - p1[0]
        d[1] = p2[1] - p1[1]
        d[2] = p2[2] - p1[2]
        #cur = resultArray.GetTuple3(i)
        #p = [0]*3
        #p[0] = cur[0] + dX
        #p[1] = cur[1] + dY
        #p[2] = cur[2] + dZ
        rigidDisplacement.SetTuple3(i,d[0],d[1],d[2])

    mesh.GetPointData().AddArray( rigidDisplacement )

    del rigidDisplacement, geometryFilter, transformFilter, transformed

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Extracts a partial surface of a mesh, randomly translated and rotated.")
    parser.add_argument("mesh", type=filepath, help="Full intra-operative mesh")
    parser.add_argument("--seed", type=int, default=None, help="Number to seed the random generator with. Zero will result in random initial seed.")
    parser.add_argument("--outputDir", type=path, default=".", help="Where to place the output mesh.")
    args = parser.parse_args()

    # Load the input mesh:
    fileType = args.mesh[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName( args.mesh )
        reader.Update() # Needed because of GetScalarRange
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() # Needed because of GetScalarRange
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() # Needed because of GetScalarRange
        mesh = reader.GetOutput()
    else:
        raise IOError("Mesh should be .vtk or .stl file")

    
    meshModified,_ = makePartialSurface( mesh, args.seed )
    print("Surface areas:", surfaceArea( mesh ), surfaceArea( meshModified ))

    append = vtkAppendFilter()
    append.SetInputData( meshModified )
    append.Update()

    if args.outputDir is None:
        args.outputDir = "."
    writer = vtkUnstructuredGridWriter()
    writer.SetFileName( os.path.join( args.outputDir, "partialSurface.vtk" ) )
    writer.SetInputData( append.GetOutput() )
    writer.Update() # Needed because of GetScalarRange



