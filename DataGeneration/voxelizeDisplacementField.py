import voxelize
from vtk import *
from utils import *

def undeform( mesh, scale=1 ):
    
    warpByVector = vtkWarpVector()
    warpByVector.SetScaleFactor(-1*scale)
    warpByVector.SetInputData( mesh )
    #warpByVector.SetInputArrayToProcess(0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_POINTS,"displacement");
    mesh.GetPointData().SetActiveVectors("displacement");
    warpByVector.Update()

    #writer = vtkUnstructuredGridWriter()
    #writer.SetFileName( "unwarped.vtk" )
    #writer.SetInputData( warpByVector.GetOutput() )
    ##writer.SetInputConnection( warpByVector.GetOutputPort() )
    #writer.Update()

    return warpByVector.GetOutput()

def calcDisplacement( mesh_initial, mesh_deformed ):

    if not mesh_initial.GetNumberOfPoints() == mesh_deformed.GetNumberOfPoints():
        raise IOError( "mesh_initial and mesh_deformed must have the same number of points!" )

    displacement = vtkDoubleArray()
    displacement.SetName( "displacement" )
    displacement.SetNumberOfComponents( 3 )
    displacement.SetNumberOfTuples( mesh_initial.GetNumberOfPoints() )

    for i in range( mesh_initial.GetNumberOfPoints() ):
        p1 = mesh_initial.GetPoint( i )
        p2 = mesh_deformed.GetPoint( i )
        displ = (p2[0]-p1[0], p2[1]-p1[1], p2[2]-p1[2])
        displacement.SetTuple3( i, displ[0], displ[1], displ[2] )

    mesh_initial.GetPointData().AddArray( displacement )

    return mesh_initial

def interpolateToGrid( mesh, grid, gridSize ):
    
    # Perform the interpolation
    interpolator = vtkPointInterpolator()
    gaussianKernel = vtkGaussianKernel()
    gaussianKernel.SetRadius( gridSize*15)
    gaussianKernel.SetSharpness(10)
    interpolator.SetSourceData( mesh )
    interpolator.SetInputData( grid )
    interpolator.SetKernel( gaussianKernel )
    interpolator.SetNullPointsStrategy(vtkPointInterpolator.CLOSEST_POINT)
    interpolator.Update()

    #output = interpolator.GetOutput()
    output = vtkStructuredGrid()
    output.DeepCopy(interpolator.GetOutput())
    del interpolator, gaussianKernel
    return output

def loadInputMesh( filename ):
    fileType = filename[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName( filename )
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        mesh = reader.GetOutput()
    else:
        raise IOError(filename + " should be .vtk, .vtu or .stl file")
    return mesh


def loadOutputGrid( filename ):
    fileType = filename[-4:].lower()
    if fileType == ".vtk":
        reader = vtkStructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        grid = reader.GetOutput()
    elif fileType == ".vts":
        reader = vtkXMLStructuredGridReader()
        reader.SetFileName( filename )
        reader.Update() 
        grid = reader.GetOutput()
    else:
        raise IOError(filename + " should be .vtk or .vts")
    return grid


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Given a (deformed) mesh with a field called \"displacement\", create a displacement field on a regular grid." )
    group = parser.add_argument_group("Mesh")
    group.add_argument("--mesh", type=filepath, help="Mesh to voxelize. Must have a point array called \"displacement\". Will first be \"undeformed\", i.e. the negative displacement will be applied as a first step (to obtain the original undeformed mesh). Mutually exclusive with mesh_initial and mesh_deformed")
    group.add_argument("--mesh_initial", type=filepath, help="Mesh to voxelize. Requires '--mesh_deformed' to be given, which must be the same mesh as this one, but deformed. Mutually exclusive with '--mesh'")
    group.add_argument("--mesh_deformed", type=filepath, help="Mesh to voxelize. Requires '--mesh_initial' to be given. Mutually exclusive with '--mesh'")
    parser.add_argument("--arrayName", type=str, default="displacement", help="Name of the generated array. Defaults to \"displacement\".")
    parser.add_argument("--outputGrid", default="voxelized.vts", help="Name of the output file to generate. If this file exists, grid size and dimensions are used from the grid and --size and --grid_size are ignored. In this case, the new array is added to the existing file.")
    parser.add_argument("--size", type=float, default=0.3, help="Size of resulting grid (ignored if --outputGrid points to an already existing grid)")
    parser.add_argument("--grid_size", type=int, default=64, help="Number of voxels per dimension (ignored if --outputGrid points to an already existing grid)")
    args = parser.parse_args()

    if args.mesh is None and args.mesh_initial is None and args.mesh_deformed is None:
        parser.error("Either --mesh or --mesh_initial and --mesh_deformed are required.")

    if (args.mesh is not None) and (args.mesh_initial is not None or args.mesh_deformed is not None):
        parser.error("--mesh is mutually exclusive with --mesh_initial and --mesh_deformed!")

    if (args.mesh_initial is None) != (args.mesh_deformed is None):
        parser.error("--mesh_initial and --mesh_deformed must both be defined!")

    # Load the output mesh:
    if os.path.exists( args.outputGrid ):
        grid = loadOutputGrid( args.outputGrid )

        if grid.GetPointData().GetArray( args.arrayName ):
            err = "The output file {} already has a field named {}!".format(args.outputGrid,args.arrayName)
            raise IOError(err)
        #args.size = grid.GetBounds()
        b = grid.GetBounds()
        args.size = b[1]-b[0]
        args.grid_size = grid.GetDimensions()[0]
    else:
        grid = voxelize.createGrid( args.size, args.grid_size )

    tf = vtkTransform()
    try:
        tf = voxelize.loadTransformationMatrix( grid )
        print("Will reuse transformation matrix found in grid:")
        print(tf)
    except:
        print("No transformation matrix found in grid. Will not apply any transformation.")

    if args.mesh is not None:
        # Load the input mesh:
        mesh = loadInputMesh( args.mesh )

        # Re-apply any transformation previously applied to the meshes:
        tfFilter = vtkTransformFilter()
        tfFilter.SetTransform( tf )
        tfFilter.SetInputData( mesh_initial )
        tfFilter.Update()
        mesh_initial = tfFilter.GetOutput()

        # Undeform the mesh according to the "displacement" array in mesh:
        mesh = undeform( mesh, scale=tf.GetMatrix().GetElement(0,0) )
    else:

        # Load the initial mesh:
        mesh_initial = loadInputMesh( args.mesh_initial )
        # Load deformed mesh:
        mesh_deformed = loadInputMesh( args.mesh_deformed )

        # Re-apply any transformation previously applied to the meshes:
        tfFilter = vtkTransformFilter()
        tfFilter.SetTransform( tf )
        tfFilter.SetInputData( mesh_initial )
        tfFilter.Update()
        mesh_initial = tfFilter.GetOutput()

        tfFilter = vtkTransformFilter()
        tfFilter.SetTransform( tf )
        tfFilter.SetInputData( mesh_deformed )
        tfFilter.Update()
        mesh_deformed = tfFilter.GetOutput()

        print( "Calculating displacement of every point" )
        mesh = calcDisplacement( mesh_initial, mesh_deformed )
    
    print( "Interpolating to grid" )
    grid = interpolateToGrid( mesh, grid, args.size/(args.grid_size-1) )

    print("Writing to {}".format( args.outputGrid ))
    writer = vtkXMLStructuredGridWriter()
    writer.SetFileName( args.outputGrid )
    writer.SetInputData( grid )
    writer.Update()



