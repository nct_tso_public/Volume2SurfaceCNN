import random
import sys
import numpy
from vtk import *
from utils import *

# Add a random force pulling on a random surface region:
def addRandomForce( mesh, triangleIDs ):

    radius = 0.01 + 0.14*random.random()
    region = getRandomSurfaceArea( mesh, radius, triangleIDs, 3 )

    # Find out how many force arrays there are already:
    forceArrays = findArraysByName( mesh.GetCellData(), "force" )
    forceArrayName = "force{:d}".format(len(forceArrays)+1)
    # Create a new force array:
    force = vtkFloatArray()
    force.SetName( forceArrayName )
    force.SetNumberOfComponents(3)
    force.SetNumberOfTuples( mesh.GetNumberOfCells() )
    force.Fill( 0 )
    mesh.GetCellData().AddArray( force )

    # Create the pressure as a random vector:
    rP = numpy.array( (random.random()-0.5, random.random()-0.5, random.random()-0.5) )
    # Calculate area of the random surface region:
    area = 0;
    for i in range( 0, len(region) ):
        c = mesh.GetCell( region[i] )
        area += c.ComputeArea()
    # rescale the manipulation
    magnitudeForce = 1.5*(1-random.random()**2)   # Higher forces more likely than smaller forces
    magnitude = magnitudeForce/area

    mag = numpy.linalg.norm(rP)
    if mag > 0:
        rP = rP/ mag*magnitude

    #print("Area of manipulation surface region: ~{} m^2".format(area))
    #print("Random force:", magnitudeForce, "Pressure:", magnitude, rP )

    # Add the pressure to the entire region:
    for i in region:
        force.SetTuple3( i, rP[0], rP[1], rP[2] )



# Add a dirichlet boundary condition (u=0) at a random surface region:
def addRandomZeroDisplacement( mesh, inputMeshSize, triangleIDs ):
    radius = 0.025 + 0.05*random.random()*inputMeshSize;
    region = getRandomSurfaceArea( mesh, radius, triangleIDs, 3 )

    if mesh.GetCellData().HasArray( "zeroDisplacement" ):   # Reuse existing array if possible
        zeroDisplacement = mesh.GetCellData().GetArray( "zeroDisplacement" )
    else:   # Otherwise create new array:
        zeroDisplacement = vtkFloatArray()
        zeroDisplacement.SetName("zeroDisplacement")
        zeroDisplacement.SetNumberOfComponents(1)
        zeroDisplacement.SetNumberOfTuples( mesh.GetNumberOfCells() )
        zeroDisplacement.Fill( 0 )
        mesh.GetCellData().AddArray( zeroDisplacement )

    for i in region:
        zeroDisplacement.SetTuple1( i, 1 )


def getCellCenter( cell ):
    paramCoords = [0]*3
    cell.GetParametricCenter( paramCoords )
    center = [0]*3
    weights = [0]*3
    subID = vtk.mutable(-1)
    cell.EvaluateLocation( subID,paramCoords,center,weights )
    return center

#def getRandomSurfaceRegion( mesh, radius, triangleIDs, minRequiredCells=None ):
#
#    # Choose a random triangle ID from the list of triangle IDs to act as the center triangle:
#    centerTriangleID = triangleIDs[random.randint(0,len(triangleIDs)-1)]
#
#    foundSuitableRegion = False
#    while not foundSuitableRegion:
#        idsOfSurfaceRegion = []
#        idsOfSurfaceRegion.append(centerTriangleID)
#       
#        center = getCellCenter( mesh.GetCell(centerTriangleID) )
#        r2 = radius**2  #radius squred
#        for i in triangleIDs:
#            c = mesh.GetCell(i)
#            ps = c.GetPoints()
#            tooFar = False
#            for j in range(0,ps.GetNumberOfPoints()):
#                p = ps.GetPoint(j)
#                dist2 = (center[0]-p[0])**2 + (center[1]-p[1])**2 + (center[2]-p[2])**2
#                if dist2 > r2:
#                    tooFar = True
#                    break
#            if not tooFar:
#                idsOfSurfaceRegion.append( i )
#        #print( "Number of triangles in region:", len(idsOfSurfaceRegion) )
#        if minRequiredCells is None or len(idsOfSurfaceRegion) >= minRequiredCells:
#            foundSuitableRegion = True
#        else:
#            radius += 0.01  # Increase radius and try again
#
#    return idsOfSurfaceRegion


def mesh2simulation( filename, seed, outputDir ):

    random.seed( seed )

    # Load the input volume:
    e = ErrorObserver()
    reader = vtkUnstructuredGridReader()
    reader.SetFileName( filename )
    reader.AddObserver( "ErrorEvent", e )
    reader.Update() # Needed because of GetScalarRange
    mesh = reader.GetOutput()
    if e.ErrorOccurred():
        raise IOError( e.ErrorMessage() )
        

    # Calculate the largest dimension:
    b = mesh.GetPoints().GetBounds()
    inputMeshSize = max( (b[1]-b[0], b[3]-b[2], b[5]-b[4] ) )
    print("Maximum dimension:", inputMeshSize)

    # Find all triangles in the mesh:
    triangleIDs = []
    for i in range(0,mesh.GetNumberOfCells()):
        if mesh.GetCell(i).GetCellType() == VTK_TRIANGLE:
            triangleIDs.append(i)
    print("Number of triangles:", len(triangleIDs))
    
    # Add random fixed displacement boundary condiiton:
    print("Adding zero displacement boundary condition")
    addRandomZeroDisplacement( mesh, inputMeshSize, triangleIDs )

    numForces = random.randint(1,3)
    for i in range(0,numForces):
        print("Adding random force")
        addRandomForce( mesh, triangleIDs )

    # Write the output mesh:
    writer = vtkUnstructuredGridWriter()
    writer.SetFileName( os.path.join( outputDir, "simulation.vtk" ) )
    writer.SetInputData( mesh )
    writer.Update()

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Add random boundary conditions to a given mesh.")
    parser.add_argument("mesh", type=filepath, help="Volume mesh on which to run the simulation.")
    parser.add_argument("outdir", type=path, default=".", help="Folder in which to save the simulation files.")
    parser.add_argument("--seed", type=int, default=None, help="Number to seed the random generator with. Leave empty for random initial seed.")
    args = parser.parse_args()

    mesh2simulation( args.mesh, args.seed, args.outdir )


