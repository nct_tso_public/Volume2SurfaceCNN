import os
from utils import *
import random
import copy
from vtk import *


elementsPerCellType = {}

BOUNDARY_ZERO_FORCE = 1
BOUNDARY_ZERO_DISPLACEMENT = 2
BOUNDARY_FORCE = 3

# Take a vtk mesh (with boundary conditions) and write files which can be used by Elmer to run the simulation
def mesh2Elmer( mesh, outputPath, seed ):

    #random.seed( seed )

    # Write the point positions:
    with open( os.path.join( outputPath, "mesh.nodes" ), "w" ) as f:
        pos = [0]*3
        for i in range(0,mesh.GetNumberOfPoints()):
            mesh.GetPoint(i,pos)
            f.write( "{:d} -1 {:f} {:f} {:f}\n".format( i+1, pos[0], pos[1], pos[2] ) )

    # Write the cells:
    numVolumeElements = 0
    body = 1
    volumeElementIDs = [-1]*mesh.GetNumberOfCells()
    with open( os.path.join( outputPath, "mesh.elements" ), "w" ) as f:
        for i in range(0,mesh.GetNumberOfCells()):
            cell = mesh.GetCell(i)
            cellType = typeForCell( cell )
            countCellType( cellType )
            if cell.GetCellType() == VTK_TETRA:
                numVolumeElements += 1
                volumeElementIDs[i] = numVolumeElements
                f.write( "{:d} {:d} {:d}".format(numVolumeElements, body, cellType) )
                for j in range(0,cell.GetNumberOfPoints()):
                    f.write( " {:d}".format(cell.GetPointId(j)+1) )
                f.write("\n")

    # Write boundaries and boundary conditions:
    zeroDisplacement = mesh.GetCellData().GetArray("zeroDisplacement")
    forces = findArraysByName( mesh.GetCellData(), "force" )
    numSurfaceElements = 0
    with open( os.path.join( outputPath, "mesh.boundary" ), "w" ) as f:
        for i in range(0,mesh.GetNumberOfCells()):
            cell = mesh.GetCell(i)
            cellType = typeForCell( cell )
            if cell.GetCellType() == VTK_TRIANGLE:
                parent = parentForCell( mesh, i );
                cell = mesh.GetCell(i)      # Workaround for cell reference changing
                # As volume nodes have new indexing, use this.
                parentAsVolumeIndex = volumeElementIDs[parent]
                boundaryIndex = BOUNDARY_ZERO_FORCE
                if zeroDisplacement.GetTuple1(i) > 0:
                    boundaryIndex = BOUNDARY_ZERO_DISPLACEMENT
                else:
                    for j in range(0,len(forces)):
                        fi = forces[j].GetTuple(i)
                        if fi[0] != 0 or fi[1] != 0 or fi[2] != 0:
                            boundaryIndex = BOUNDARY_FORCE + j
                            break
                f.write( "{:d} {:d} {:d} 0 {:d}".format(i+1,boundaryIndex,parentAsVolumeIndex,cellType) )
                for j in range(0,cell.GetNumberOfPoints()):
                    f.write( " {:d}".format(cell.GetPointId(j)+1) )
                f.write("\n")
                numSurfaceElements += 1

    with open( os.path.join( outputPath, "mesh.header" ), "w" ) as f:
        f.write( "{} {} {}\n".format( mesh.GetNumberOfPoints(), numVolumeElements, numSurfaceElements ) )
        f.write( "{}\n".format( len(elementsPerCellType) ) )
        for cellType, num in elementsPerCellType.items():
            f.write( "{} {}\n".format( cellType, num ) )



# Search for the tetrahedron which the given triangle is connected to:
def parentForCell( mesh, triangleIndex ):

    tri = mesh.GetCell( triangleIndex )
    neighborCellIds = vtkIdList()
    mesh.GetCellNeighbors(triangleIndex, tri.GetPointIds(), neighborCellIds);
    return neighborCellIds.GetId(0)
#    pID0 = tri.GetPointId(0);
#    pID1 = tri.GetPointId(1);
#    pID2 = tri.GetPointId(2);
#
#    for i in range(0, mesh.GetNumberOfCells()):
#        c = mesh.GetCell(i)
#        if c.GetCellType() != VTK_TRIANGLE:
#            matchingPoints = 0
#            for j in range(0, c.GetNumberOfPoints()):
#                pID = c.GetPointId(j)
#                if pID0 == pID or pID1 == pID or pID2 == pID:
#                    matchingPoints += 1
#            if matchingPoints == 3:
#                return i
#    return 0



def typeForCell( cell ):
    if cell.GetCellType() == VTK_TRIANGLE:
        return 303
    if cell.GetCellType() == VTK_QUAD:
        return 404
    if cell.GetCellType() == VTK_HEXAHEDRON:
        return 808
    if cell.GetCellType() == VTK_TETRA:
        return 504
    return 0

def countCellType( cellType ):
    global elementsPerCellType
    if not cellType in elementsPerCellType:
        elementsPerCellType[cellType] = 0
    elementsPerCellType[cellType] += 1
      

    
def simulation2Elmer( mesh, outputPath, youngsModulus=3e3, maxForces=99, forcePrefactor=1 ):

    forces = findArraysByName( mesh.GetCellData(), "force" )

    # If we're being told to only use some of the force boundary conditions, delete the others here:
    while len(forces) > maxForces:
        del forces[len(forces)-1]

    forceValues = []
    for j in range(0,len(forces)):
        for i in range( 0, mesh.GetNumberOfCells() ):
            fi = forces[j].GetTuple(i)

            # Possibly scale down force:
            fi = (fi[0]*forcePrefactor,fi[1]*forcePrefactor,fi[2]*forcePrefactor)

            if fi[0] != 0 or fi[1] != 0 or fi[2] != 0:
                forceValues.append(fi)
                break

    with open( os.path.join( outputPath, "case.sif" ), "w" ) as f:
    # Write header:
        f.write("Header\n")
        f.write("\tCHECK KEYWORDS Warn\n")
        f.write("\tMesh DB \".\" \".\"\n")
        f.write("\tInclude Path \"\"\n")
        f.write("\tResults Directory \"result\"\n")
        f.write("End\n\n")

    # Simulation:
        f.write("Simulation\n")
        f.write("\tMax Output Level = 3\n")
        f.write("\tCoordinate System = Cartesian\n")
        f.write("\tCoordinate Mapping(3) = 1 2 3\n")
        f.write("\tSimulation Type = Steady state\n")
        f.write("\tSteady State Max Iterations = 1\n")
        f.write("\tSolver Input File = case.sif\n")
        f.write("\tPost File = case.vtu\n")
        f.write("End\n\n")

    # Solver:
        f.write("Solver 1\n")
        f.write("\tEquation = Non Linear elasticity\n")
        f.write("\tProcedure = \"ElasticSolve\" \"ElasticSolver\"\n")
        #f.write("\tVariable = -dofs 3 Displacement\n")
        #f.write("\tExec Solver = Always\n")
        #f.write("\tStabilize = True\n")
        #f.write("\tBubbles = False\n")
        #f.write("\tLumped Mass Matrix = False\n")
        #f.write("\tOptimize Bandwidth = True\n")
        #f.write("\tSteady State Convergence Tolerance = 1.0e-5\n")
        #f.write("\tNonlinear System Convergence Tolerance = 1.0e-5\n")
        #f.write("\tNonlinear System Max Iterations = 250\n")
        #f.write("\tNonlinear System Newton After Iterations = 3\n")
        #f.write("\tNonlinear System Newton After Tolerance = 1.0e-3\n")
        #f.write("\tNonlinear System Relaxation Factor = 1\n")
        #f.write("\tLinear System Solver = Iterative\n")
        #f.write("\tLinear System Iterative Method = GCR\n")
        #f.write("\tLinear System Max Iterations = 250\n")
        #f.write("\tLinear System Convergence Tolerance = 1.0e-6\n")
        #f.write("\tBiCGstabl polynomial degree = 2\n")
        #f.write("\tLinear System Preconditioning = ILU1\n")
        #f.write("\tLinear System ILUT Tolerance = 1.0e-3\n")
        #f.write("\tLinear System Abort Not Converged = False\n")
        #f.write("\tLinear System Residual Output = 1\n")
        #f.write("\tLinear System Precondition Recompute = 1\n")
        f.write("\tVariable = Displacement\n")
        f.write("\tVariable DOFs = 3\n")

        f.write("\tLinear System Solver = Iterative\n")
        f.write("\tLinear System Iterative Method = BiCGStabl\n")
        f.write("\tLinear System Max Iterations = 250\n")
        f.write("\tLinear System Convergence Tolerance = 1.0e-7\n")
        f.write("\tBiCGstabl polynomial degree = 2\n")
        f.write("\tLinear System Preconditioning = ILU1\n")
        #f.write("\tLinear System ILUT Tolerance = 1.0e-3\n")
        #f.write("\tLinear System Abort Not Converged = False\n")
        #f.write("\tLinear System Residual Output = 1\n")
        f.write("\tLinear System Precondition Recompute = 1\n")

        f.write("\tNonlinear System Newton After Iterations = 50\n")
        f.write("\tNonlinear System Newton After Tolerance = 1.0e-3\n")
        f.write("\tNonlinear System Max Iterations = 100\n")
        f.write("\tNonlinear System Convergence Tolerance = 1.0e-4\n")
        f.write("\tNonlinear System Relaxation Factor = 1.0\n")
        f.write("\tSteady State Convergence Tolerance = 1.0e-5\n")

        f.write("\tNeo-Hookean Material = Logical True\n")
        f.write("\tCalculate Stresses = Logical True\n")
        f.write("\tCalculate Strains = Logical True\n")

        f.write("End\n\n")

    # Equation:
        f.write("Equation 1\n")
        f.write("\tName = \"Elasticity\"\n")
        
            #tCalculate Stresses = True\n" <<
        f.write("\tActive Solvers(1) = 1\n")
        f.write("\tNonlinear Elasticity = Logical True\n")
        f.write("End\n\n")

        f.write("Body 1\n")
        f.write("\tTarget Bodies(1) = 1\n")
        f.write("\tEquation = 1\n")
        f.write("\tMaterial = 1\n")
        f.write("End\n\n")

        f.write("Material 1\n")
        f.write("\tName = \"Liver (Human, approx)\"\n")
        f.write("\tDensity = 1060.0\n")
        f.write("\tPoisson ratio = 0.35\n")
        f.write("\tYoungs modulus = {:f}\n".format(youngsModulus) )
        f.write("End\n\n")

        f.write("Boundary Condition 2\n")
        f.write("\tTarget Boundaries(1) = 2\n")
        f.write("\tName = \"ZeroU\"\n")
        f.write("\tDisplacement 3 = 0\n")
        f.write("\tDisplacement 2 = 0\n")
        f.write("\tDisplacement 1 = 0\n")
        f.write("End\n\n")

        for j in range(0, len(forces)):
            f.write("Boundary Condition {:d}\n".format(3+j))
            f.write("\tTarget Boundaries(1) = {:d}\n".format(3+j))
            f.write("\tName = \"Pull\"\n")
            f.write("\t!A 'Force' in a boundary condition is really a pressure\n")
            f.write("\t!This means that the values given here are in Pascal\n")
            f.write("\t!Don't ask me why they call it a 'Force'... :/\n")
            f.write("\tForce 1 = {:f}\n".format(forceValues[j][0]))
            f.write("\tForce 2 = {:f}\n".format(forceValues[j][1]))
            f.write("\tForce 3 = {:f}\n".format(forceValues[j][2]))
            f.write("End\n\n")

def convert( filename, seed, maxForces=99, forcePrefactor=1 ):

    outputPath = os.path.dirname( filename )
    
    # Load the input volume:
    reader = vtkUnstructuredGridReader()
    reader.SetFileName( filename )
    reader.Update() # Needed because of GetScalarRange
    mesh = reader.GetOutput()
    if mesh.GetNumberOfPoints() <= 0:    # Loaded successfully?
        raise IOError("Could not read {}".format(filename))

    mesh2Elmer( mesh, outputPath, seed )

    #simulation2Elmer( mesh, outputPath, 3e3 )
    #simulation2Elmer( mesh, outputPath, 2.5e3 + random.random()*2.5e3 )
    #simulation2Elmer( mesh, outputPath, 2e3 + random.random()*4e3, maxForces=maxForces, forcePrefactor=forcePrefactor )
    simulation2Elmer( mesh, outputPath, 500 + random.random()*4500, maxForces=maxForces, forcePrefactor=forcePrefactor )


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Convert vtk file to an Elmer simulation setup.")
    parser.add_argument("mesh", type=filepath, help="Simulation mesh with boundary conditions on which to run the simulation.")
    parser.add_argument("--seed", type=int, default=0, help="Number to seed the random generator with. Zero will result in random initial seed.")
    args = parser.parse_args()

    convert( args.mesh, args.seed )
