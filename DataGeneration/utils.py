import os
import math
import random
from vtk import *

# Check if string is a valid path. If not, try to create it. If not, fail.
def path(string):
    if os.path.isdir(string):
        return string
    else:
        try:
            os.makedirs(string)
            return string
        except:
            raise NotADirectoryError(string)

# Check if string is a valid file. If not, fail.
def filepath(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)

def directoryForIndex( index, outdir ):
    d = os.path.join( outdir, "{:05d}".format(index))
    if not os.path.exists( d ):
        os.makedirs( d )
    return d

# Search for all arrays in data which contain "string" in their name and return them.
def findArraysByName( data, string ):
    foundArrays = []
    for i in range(0, data.GetNumberOfArrays() ):
        if string in data.GetArray(i).GetName():
            foundArrays.append( data.GetArray(i) )
    return foundArrays

def extractSurface( inputMesh ):
    #triangleFilter = vtkTriangleFilter()
    #triangleFilter.SetInputData( inputMesh )
    #triangleFilter.Update()
    #surface = triangleFilter.GetOutput()

    surfaceFilter = vtkDataSetSurfaceFilter()
    surfaceFilter.SetInputData( inputMesh )
    surfaceFilter.Update()
    surface = surfaceFilter.GetOutput()

    return surface


def getRandomSurfaceArea( mesh, radius, triangleIDs, minRequiredCells=0, centerTriangleID=None ):
    
    # Final area should roughly have this size:
    targetAreaSize = math.pi*radius**2

    # Choose a random triangle ID from the list of triangle IDs to act as the center triangle:
    if centerTriangleID == None:
        centerTriangleID = triangleIDs[random.randint(0,len(triangleIDs)-1)]
    
    centerCell = mesh.GetCell( centerTriangleID )

    region = []
    area = 0

    borderCells = [centerTriangleID]
    checkedCells = []

    while area < targetAreaSize:
        if len(borderCells) == 0:
            break

        newBorderCells = []
        for borderCellID in borderCells:
            checkedCells.append(borderCellID)

            idList = vtkIdList()
            newNeighbours = []

            edge1 = mesh.GetCell( borderCellID ).GetEdge( 0 ).GetPointIds()
            mesh.GetCellNeighbors( borderCellID, edge1, idList )
            newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
            edge2 = mesh.GetCell( borderCellID ).GetEdge( 1 ).GetPointIds()
            mesh.GetCellNeighbors( borderCellID, edge1, idList )
            newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
            edge3 = mesh.GetCell( borderCellID ).GetEdge( 2 ).GetPointIds()
            mesh.GetCellNeighbors( borderCellID, edge1, idList )
            newNeighbours += [idList.GetId(i) for i in range(0,idList.GetNumberOfIds())]
            #for n in newNeighbours:
            #    print(n, mesh.GetCell(n).GetCellType())

            for newID in newNeighbours:
                if newID not in checkedCells and newID not in borderCells and not newID in newBorderCells:
                    if mesh.GetCell( newID ).GetCellType() == VTK_TRIANGLE:
                        newBorderCells.append( newID )

            area += mesh.GetCell( borderCellID ).ComputeArea()
            region.append( borderCellID )
          
        # After traversing the list, delete old borderCell list and replace it by the new one:
        borderCells = newBorderCells

    if len(region) >= minRequiredCells:
        return region

    return getRandomSurfaceArea( mesh, radius + 0.01, triangleIDs, minRequiredCells )

def removeUnwantedArrays( data, arraysToKeep ):

    unwantedArrayFound = True
    while unwantedArrayFound:
        unwantedArrayFound = False
        for i in range(0, data.GetNumberOfArrays()):
            if not data.GetArray(i).GetName() in arraysToKeep:
                unwantedArrayFound = True
                data.RemoveArray(i)
                break

class ErrorObserver:

   def __init__(self):
       self.__ErrorOccurred = False
       self.__ErrorMessage = None
       self.CallDataType = 'string0'

   def __call__(self, obj, event, message):
       self.__ErrorOccurred = True
       self.__ErrorMessage = message

   def ErrorOccurred(self):
       occ = self.__ErrorOccurred
       self.__ErrorOccurred = False
       return occ

   def ErrorMessage(self):
       return self.__ErrorMessage

def makeSingleFloatArray( name, val ):
    arr = vtkFloatArray()
    arr.SetNumberOfTuples(1)
    arr.SetNumberOfComponents(1)
    arr.SetTuple1(0,val)
    arr.SetName(name)
    return arr

def surfaceArea( mesh ):
    area = 0
    for i in range(mesh.GetNumberOfCells()):
        if mesh.GetCell(i).GetCellType() == VTK_TRIANGLE:
            p0 = mesh.GetCell(i).GetPoints().GetPoint(0)
            p1 = mesh.GetCell(i).GetPoints().GetPoint(1)
            p2 = mesh.GetCell(i).GetPoints().GetPoint(2)
            a = mesh.GetCell(i).TriangleArea(p0,p1,p2)
            area += a

    areaArr = makeSingleFloatArray( "surfaceArea", area )
    mesh.GetFieldData().AddArray(areaArr)

    return area

def getMaximumOfArray( arr, fieldData ):
    # First get the maximum value (norm) in the array from the source data
    maximum = arr.GetMaxNorm()
    # Make an array holding only this one maximum value:
    fieldArr = makeSingleFloatArray( arr.GetName() + "_max", maximum )
    # Append the value to the field data
    fieldData.AddArray( fieldArr )
    return maximum

# Sum up two arrays element wise and return an array with the result.
# arr1 and arr2 must have the same number of tuples and components!
def sumOfArrays( arr1, arr2, resultArrayName ):

    tuples = arr1.GetNumberOfTuples()
    components = arr1.GetNumberOfComponents()
    resArr = vtkDoubleArray()
    resArr.SetNumberOfComponents( components )
    resArr.SetNumberOfTuples( tuples )
    resArr.SetName( resultArrayName )
    for i in range(tuples):
        for j in range(components):
            s = arr1.GetComponent(i,j) + arr2.GetComponent(i,j) 
            resArr.SetComponent(i,j,s)

    return resArr


def unstructuredGridToPolyData( ug ):
    geometryFilter = vtkGeometryFilter()
    geometryFilter.SetInputData( ug )
    geometryFilter.Update()
    return geometryFilter.GetOutput()

def addVertsToPolyData( pd ):
    
    verts = vtkCellArray()
    for i in range(pd.GetNumberOfPoints()):
        verts.InsertNextCell( 1, (i,) )

    pd.SetVerts(verts)

