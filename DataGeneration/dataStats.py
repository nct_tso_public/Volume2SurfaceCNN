import collections
import os
from vtk import *
from operator import attrgetter
import numpy
from utils import *
import statistics
import pickle

import matplotlib.pyplot as plt


Stats = collections.namedtuple("Stats", ["name",
    "maxStress", "avgStress",
    "maxDispl","avgDispl",
    "maxRigidDispl","avgRigidDispl",
    "maxTotalDispl","avgTotalDispl",
    "visibleAmount",
    "voxels"])

def visibleSurfaceAmount( voxelizedMesh ):

    # Find number of voxels which lie close to the surface (rough measure for the surface area)
    #thresh = vtkThreshold()
    #thresh.SetInputData( voxelizedMesh )
    #thresh.SetInputArrayToProcess( 0,0,0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "preoperativeSurface" )
    #thresh.ThresholdBetween( -0.01, 0.01 )
    #geometryFilter = vtkGeometryFilter()
    #geometryFilter.SetInputConnection( thresh.GetOutputPort() )
    #geometryFilter.Update()
    #numPreoperativeSurfacePoints = geometryFilter.GetOutput().GetNumberOfPoints()

    ## Find number of voxels which lie close to the surface (rough measure for the surface area)
    #thresh = vtkThreshold()
    #thresh.SetInputData( voxelizedMesh )
    #thresh.SetInputArrayToProcess( 0,0,0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "intraoperativeSurface" )
    #thresh.ThresholdBetween( 0, 0.01 )
    #geometryFilter = vtkGeometryFilter()
    #geometryFilter.SetInputConnection( thresh.GetOutputPort() )
    #geometryFilter.Update()
    #numIntraoperativeSurfacePoints = geometryFilter.GetOutput().GetNumberOfPoints()

    #if numPreoperativeSurfacePoints > 0:
    #    visibleAmount = numIntraoperativeSurfacePoints/numPreoperativeSurfacePoints
    #else:
    #    visibleAmount = 0
    visibleAmount = voxelizedMesh.GetFieldData().GetArray("visibleSurfaceAmount").GetTuple(0)[0]
    
    return visibleAmount

# Retrieve maximum and mean stress:
def getStressMetrics( deformedMesh ):
    #displacement = deformedMesh.GetPointData().GetArray("displacement")
    vonMisesStress = deformedMesh.GetPointData().GetArray("vonmises")
    #ds = []
    vMs = []
    for i in range(deformedMesh.GetNumberOfPoints()):
        #d = displacement.GetTuple(i)
        #dMag = numpy.linalg.norm( d )
        #ds.append(dMag)
        vM = vonMisesStress.GetTuple(i)[0]
        vMs.append(vM)

    #return max(ds), sum(ds)/len(ds), max(vMs), sum(vMs)/len(vMs)
    return max(vMs), sum(vMs)/len(vMs)

def getDisplacementMetrics( voxelizedMesh ):
    displacement = voxelizedMesh.GetPointData().GetArray( "displacement" )
    rigidDisplacement = voxelizedMesh.GetPointData().GetArray( "rigidDisplacement" )
    totalDisplacement = sumOfArrays( displacement, rigidDisplacement, "totalDisplacement" )
    preoperativeSurface = voxelizedMesh.GetPointData().GetArray( "preoperativeSurface" )

    maxDispl = 0
    sumDispl = 0
    maxRigidDispl = 0
    sumRigidDispl = 0
    maxTotalDispl = 0
    sumTotalDispl = 0
    voxels = 0
    for i in range(voxelizedMesh.GetNumberOfPoints()):
        if preoperativeSurface.GetTuple(i)[0] <= 0:
            d = displacement.GetTuple(i)
            dMag = numpy.linalg.norm( d )
            maxDispl = max( (dMag,maxDispl) )
            sumDispl += dMag

            d = rigidDisplacement.GetTuple(i)
            dMag = numpy.linalg.norm( d )
            maxRigidDispl = max( (dMag,maxRigidDispl) )
            sumRigidDispl += dMag

            d = totalDisplacement.GetTuple(i)
            dMag = numpy.linalg.norm( d )
            maxTotalDispl = max( (dMag,maxTotalDispl) )
            sumTotalDispl += dMag

            voxels += 1

    if voxels == 0:
        return 0, 0, 0, 0, 0, 0, 0
    else:
        return maxDispl, sumDispl/voxels, maxRigidDispl, sumRigidDispl/voxels, maxTotalDispl, sumTotalDispl/voxels, voxels


def calcStats( name, deformedMesh, voxelizedMesh ):
    maxStress, avgStress = getStressMetrics( deformedMesh )
    visibleAmount = visibleSurfaceAmount( voxelizedMesh )
    maxDispl, avgDispl, maxRigidDispl, avgRigidDispl, maxTotalDispl, avgTotalDispl, voxels = getDisplacementMetrics( voxelizedMesh )
    stats = Stats( name,
            maxStress, avgStress,
            maxDispl, avgDispl,
            maxRigidDispl, avgRigidDispl,
            maxTotalDispl, avgTotalDispl,
            visibleAmount,
            voxels )
    return stats

def appendStats( voxelizedMesh, stats ):
    fieldData = vtkFieldData()

    arrOrig = vtkDoubleArray()
    arrOrig.SetNumberOfComponents(1)
    arrOrig.SetNumberOfTuples(1)

    #arr = vtkDoubleArray()
    #arr.DeepCopy(arrOrig)
    #arr.SetName( "visibleSurfaceAmount" )
    #arr.SetTuple1(0,curStats.visibleAmount)
    #fieldData.AddArray( arr )

    arr = vtkDoubleArray()
    arr.DeepCopy(arrOrig)
    arr.SetName( "maxNonRigidDispl" )
    arr.SetTuple1(0,curStats.maxDispl)
    fieldData.AddArray( arr )

    arr = vtkDoubleArray()
    arr.DeepCopy(arrOrig)
    arr.SetName( "meanNonRigidDispl" )
    arr.SetTuple1(0,curStats.meanDispl)
    fieldData.AddArray( arr )

    arr = vtkDoubleArray()
    arr.DeepCopy(arrOrig)
    arr.SetName( "maxStress" )
    arr.SetTuple1(0,curStats.maxStress)
    fieldData.AddArray( arr )

    arr = vtkDoubleArray()
    arr.DeepCopy(arrOrig)
    arr.SetName( "meanStress" )
    arr.SetTuple1(0,curStats.meanStress)
    fieldData.AddArray( arr )

    voxelizedMesh.SetFieldData(fieldData)

def stats2Str( s ):
    string = "{}: ({:.1f}% visible)".format(s.name, s.visibleAmount*100)
    string += "\n\tNon-rigid displacement: {:.3f} m (max: {:.3f} m)".format( s.avgDispl, s.maxDispl )
    string += "\n\tRigid displacement: {:.3f} m (max: {:.3f} m)".format( s.avgRigidDispl, s.maxRigidDispl )
    string += "\n\tTotal displacement: {:.3f} m (max: {:.3f} m)".format( s.avgTotalDispl, s.maxTotalDispl )
    return string

def saveStats( stats, path ):
    filePath = os.path.join( path, "stats.pkl" )
    print( "Saving stats to:", filePath ) 
    with open( filePath, 'wb') as f:
        pickle.dump( stats, f, pickle.HIGHEST_PROTOCOL )

def loadStats( path ):
    filePath = os.path.join( path, "stats.pkl" )
    print( "Loading stats from:", filePath ) 
    with open( filePath, 'rb') as f:
        return pickle.load( f )

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Convert vtk file to an Elmer simulation setup.")
    parser.add_argument("path", type=str, help="Folder in which the outputs of generate.py can be found.")
    #parser.add_argument("--writeStats", action="store_true", help="Write the stats back to field arrays of the voxelized file.")
    parser.add_argument("--num", type=int, default=-1, help="Number of samples to parse. Default: All in directory.")
    parser.add_argument("--startNum", type=int, default=0, help="ID of first sample to parse. Default: 0")
    parser.add_argument("--maxDisplacement", type=float, default=9999, help="Maximum allowed displacement")
    parser.add_argument("--loadStats", action="store_true", help="Write the stats back to field arrays of the voxelized file.")
    args = parser.parse_args()

    outFile = os.path.join( args.path, "stats.pkl" )
    if os.path.exists( outFile ) and not args.loadStats:
        print("Warning: {} exists. Overwrite? [y/n]".format( outFile ))
        answer = input()
        if not (answer == 'y' or answer == 'Y'):
            print("Aborting.")
            exit()


    if not args.loadStats:
        sims = os.listdir(args.path)
        stats = []
        for s in sorted(sims):
            simResultFile = os.path.join( args.path, s, "sim", "result", "case_t0001.vtu" )
            voxelFile = os.path.join( args.path, s, "voxelized.vts" )
    
            try:
                index = int(s)
            except:
                continue
            if index >= args.startNum and (args.num == -1 or index < args.num+args.startNum):

                if os.path.exists( simResultFile ):
                    # Load the mesh:
                    reader = vtkXMLUnstructuredGridReader()
                    reader.SetFileName( simResultFile )
                    reader.Update()
                    mesh = reader.GetOutput()

                    # Load the voxel mesh:
                    reader = vtkXMLStructuredGridReader()
                    reader.SetFileName( voxelFile )
                    reader.Update()
                    voxelized = reader.GetOutput()

                    if voxelized.GetFieldData().GetArray("displacement_max").GetTuple(0)[0] <= args.maxDisplacement:
                        curStats = calcStats( s, mesh, voxelized )
                        print(stats2Str(curStats))
                        #if args.writeStats:
                        #    # Append the stats to the FieldData of the voxelized mesh:
                        #    appendStats( voxelized, curStats )

                        #    # Write the modified voxel mesh:
                        #    writer = vtkXMLStructuredGridWriter()
                        #    writer.SetFileName( voxelFile )
                        #    writer.SetInputData( voxelized )
                        #    writer.Update()
                        if curStats.voxels > 0:
                            stats.append(curStats)
                    else:
                        print("Skipping sample ", index)
    else:
        stats = loadStats( args.path )

    if len( stats ) == 0:
        print("Could not find any files. Make sure you are passing in a folder in which the simulation data sets lie (including the sim/result/case_t0001.vtu files)")
        exit()

    if not args.loadStats:
        saveStats( stats, args.path )

    #maxDispl = max( stats, key=attrgetter("maxDispl") ).maxDispl
    #maxStress = max( stats, key=attrgetter("maxStress") ).maxStress

    avgDispls = [s.avgDispl for s in stats]
    maxDispls = [s.maxDispl for s in stats]
    avgRigidDispls = [s.avgRigidDispl for s in stats]
    maxRigidDispls = [s.maxRigidDispl for s in stats]
    avgTotalDispls = [s.avgTotalDispl for s in stats]
    maxTotalDispls = [s.maxTotalDispl for s in stats]
    maxStresses = [s.maxStress for s in stats]
    avgStresses = [s.avgStress for s in stats]
    visibleAmounts = [s.visibleAmount for s in stats]

    plt.hist( avgDispls )
    plt.savefig("avgDisplacements.eps")
    plt.cla()

    plt.hist( maxDispls )
    plt.savefig("maxDisplacements.eps")
    plt.cla()

    plt.hist( avgRigidDispls )
    plt.savefig("avgRigidDisplacements.eps")
    plt.cla()

    plt.hist( maxRigidDispls )
    plt.savefig("maxRigidDisplacements.eps")
    plt.cla()

    plt.hist( avgTotalDispls )
    plt.savefig("avgTotalDisplacements.eps")
    plt.cla()

    plt.hist( maxTotalDispls )
    plt.savefig("maxTotalDisplacements.eps")
    plt.cla()

    plt.hist( visibleAmounts )
    plt.savefig("visibleAmounts.eps")
    plt.cla()

    print("Number of meshes found:", len(stats))
    print("Non-rigid displacement:\n\tAbsolute Max: {:.3f} m\n\tAverage Max: {:.3f} m\n\tAvg.: {:.3f} m".format(
        max(maxDispls),sum(maxDispls)/len(maxDispls),float(sum(avgDispls)/len(avgDispls))))
    print("Rigid displacement:\n\tMax: {:.3f} m\n\tAvg.: {:.3f} m".format(  \
        max(maxRigidDispls),sum(avgRigidDispls)/len(avgRigidDispls)))
    print("Total displacemnt:\n\tMax: {:.3f} m\n\tAvg.: {:.3f} m".format(   \
        max(maxTotalDispls),sum(avgTotalDispls)/len(avgTotalDispls)))
    print("Von Mises stress:\n\tMax: {:.3f}\n\tAvg.: {:.3f}".format(    \
        max(maxStresses),sum(avgStresses)/len(avgStresses)))

    print("Visible Amount:\n\tAvg.: {:.3f}\n\tMedian: {:.3f}".format(    \
        sum(visibleAmounts)/len(visibleAmounts),statistics.median(visibleAmounts) ) )

    #print("Min/Max visibleAmounts:", min(visibleAmounts), max(visibleAmounts))
