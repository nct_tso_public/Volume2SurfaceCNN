import argparse
from utils import *
import statistics

##################################################
## Handle input arguments

parser = argparse.ArgumentParser(description="Test model on a series of data samples.")
parser.add_argument("dataFolder", type=path, help="Folder in which the samples lie")
parser.add_argument("--modelFolder", required=True, type=path, help="Folder in which the model is stored")
parser.add_argument("--outputFolder", required=True, type=path, help="Folder in which to save the results and models")
parser.add_argument("--num", type=int, default=200, help="Amount of data samples to load from [dataFolder]")
parser.add_argument("--startNum", type=int, default=0, help="First sample to take from [dataFolder]")
args = parser.parse_args()

##################################################
## Make backup copies, write potentionally important info
#import glob, shutil
#import tensorboardX

# Make backup copy of data:
#for f in glob.glob("./*.py"):
    #print("Making backup copy of:", f)
    #shutil.copy( f, args.outputFolder )

# Set up TensorboardX:
#trainingLog = tensorboardX.SummaryWriter(os.path.join(args.outputFolder, "logs"), write_to_disk=True )

##################################################
## Set up training and testing data:
import data
import torch
import torch.nn as nn

numTest = int(args.num)
testData = data.VoxelizedDataset( args.dataFolder, False, startIndex=args.startNum, num=numTest )
testData.silent = False
testData.setMaximumAllowedDisplacement( 0.2 )
testData.setMinimumAllowedVisSurface( 0.1 )
testData.prune()
testData.retryOnLoadingFailure = False
print("Test data loaded. ({} samples).".format(len(testData)))

##################################################
## Set up model:
import model
import checkpoint

print("Loading model")
model = model.Model().cuda()
#x1 = torch.randn(1,1,64,64,64).cuda()
#x2 = torch.randn(1,1,64,64,64).cuda()
#trainingLog.add_graph( model, (x1,x2) )

checkpoint.load( args.modelFolder, model )

##################################################
## Test:

import test
import numpy

#surfaceLoss = SurfaceLoss( voxelsPerSide=64, size=0.3 )
stats = test.testModel( model, testData,
        saveResults=True, outputFolder=args.outputFolder,
        verbose=True,
        testTiming=True )#, surfaceLoss=surfaceLoss )

import pickle
import visualize

filename = os.path.join( args.outputFolder, "results.pkl" )
print("Writing results to:", filename)
pickle.dump( stats, open( filename, 'wb' ) )

try:
    visualize.calcStats( stats )
    visualize.generatePlots( stats, args.outputFolder )
except:
    print("Could not calculate statistics. Maybe there is no ground truth displacement known?")

#maxVal = -1
#maxInd = -1
#if len(stats) > 0:
#  for i in range(len(stats)):
#      if maxVal < stats[i]["meanDisplacementErr"]:
#          maxVal = stats[i]["meanDisplacementErr"]
#          maxInd = i

