import os,sys,inspect
# Add "DataGeneration" folder to search path:
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, os.path.join(parent_dir, "DataGeneration") )
from utils import *
from vtk import *
import numpy
import voxelize
import statistics

def applyTransformation( dataset, tf ):
    for i in range( dataset.GetPointData().GetNumberOfArrays() ):
        arr = dataset.GetPointData().GetArray(i)
        if arr.GetNumberOfComponents() == 3:
            for j in range(arr.GetNumberOfTuples()):
                data = arr.GetTuple3(j)
                transformed = tf.TransformVector( data )
                arr.SetTuple3(j,transformed[0],transformed[1],transformed[2])
        

def isInternalPoint( mesh, pointID ):
    cellIDs = vktIsList()
    output.GetPointCells( pointID, cellIDs )
    print("Checking IDs", cellIDs)
    for i in range(cellIDs.GetNumberOfIds()):
        cellID = cellIDs.GetId(i)
        cell = mesh.GetCell(i)
        print(cell)
        return False
    return True

if __name__ == "__main__":

    import argparse
    import statistics

    parser = argparse.ArgumentParser(description="Apply a displacement field to a mesh. Note: This assumes the mesh is completely inside the displacement field!")
    parser.add_argument("mesh", type=filepath, help="Mesh or point cloud to displace. Needs to hold at least some points.")
    parser.add_argument("displacementField", type=filepath, help="File in which displacement field lies. This should be a .vts file with the field being a point data array.")
    parser.add_argument("--displacementFieldName", type=str, default="estimatedDisplacement", help="Array name of the displacement field")
    parser.add_argument("outputMesh", type=str, help="Output mesh")
    parser.add_argument("--reference", type=str, help="Optional reference/ground truth file with the displaced mesh. If this is given, errors will be calculated. This script assumes that the points in the reference mesh or point cloud correspond to the displaced versions of the points given in the 'mesh' parameter!")
    parser.add_argument("--verbose", action="store_true")
    args = parser.parse_args()

    # Load the input mesh:
    fileType = args.mesh[-4:].lower()
    if fileType == ".stl":
        reader = vtkSTLReader()
        reader.SetFileName( args.mesh )
        reader.Update()
        mesh = reader.GetOutput()
    elif fileType == ".vtk":
        reader = vtkUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtu":
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        mesh = reader.GetOutput()
    elif fileType == ".vtp":
        reader = vtkXMLPolyDataReader()
        reader.SetFileName( args.mesh )
        reader.Update() 
        append = vtkAppendFilter()
        append.SetInputData( reader.GetOutput() )
        append.Update()
        mesh = append.GetOutput()
    elif fileType == ".txt":
        with open( args.mesh ) as f:
            pts = vtkPoints()
            verts = vtkCellArray()
            i = 0
            for l in f:
                l = l.replace(',', " ")
                x,y,z = l.split(" ", 3)
                x = float(x)
                y = float(y)
                z = float(z)
                pts.InsertNextPoint( x, y, z )
                verts.InsertNextCell( 1, (i,) )
                i = i+1
            mesh = vtkPolyData()
            mesh.SetPoints( pts )
            mesh.SetVerts( verts )

        inputIsPointCloud = True
    else:
        raise IOError("Mesh should be .vtk, .vtu, .vtp, .txt or .stl file")
    
    # Load the displacement field:
    fileType = args.displacementField[-4:].lower()
    if fileType == ".vts":
        reader = vtkXMLStructuredGridReader()
        reader.SetFileName( args.displacementField )
        reader.Update() 
        field = reader.GetOutput()
    else:
        raise IOError("DeformationField should be .vts file")

    print( "Mesh points:", mesh.GetNumberOfPoints() )
    print( "Field points:", field.GetNumberOfPoints() )


    # In case the field data was transformed, also transform the test data:
    scale = 1 # default
    try:
        tf = voxelize.loadTransformationMatrix( field )
        tf.Inverse()
        print("Applying transform")
        tfFilter = vtkTransformFilter()
        tfFilter.SetTransform( tf )
        tfFilter.SetInputData( field )
        tfFilter.Update()
        field = tfFilter.GetOutput()
        
        # Apply transformation also to all vector fields:
        applyTransformation( field, tf )
        
        scale = tf.GetMatrix().GetElement(0,0)

    except Exception as e:
        print(e)
        print("Could not find or apply transformation. Skipping.")
      


    writer = vtkXMLStructuredGridWriter()
    writer.SetInputData( field )
    writer.SetFileName( os.path.join( os.path.dirname( args.outputMesh ), "field.vts" ) )
    writer.Update()
    print("Written1")

    # Threshold to ignore all points outside of field:
    threshold = vtkThreshold()
    threshold.SetInputArrayToProcess(0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "preoperativeSurface");
    threshold.ThresholdByLower(0)
    threshold.SetInputData( field )
    threshold.Update()
    fieldInternal = threshold.GetOutput()
    

    print("Scale", scale)

    kernel = vtkGaussianKernel()
    kernel.SetRadius(0.01*scale)
    kernel.SetKernelFootprintToRadius()
    #kernel.SetKernelFootprintToNClosest()
    #kernel.SetNumberOfPoints( 4 )

    interpolator = vtkPointInterpolator()
    interpolator.SetKernel( kernel )
    interpolator.SetNullPointsStrategyToMaskPoints()
    interpolator.SetValidPointsMaskArrayName( "validInternalPoints" )
    #interpolator.SetNullPointsStrategyToClosestPoint()
    interpolator.SetSourceData( fieldInternal )
    interpolator.SetInputData( mesh )
    interpolator.Update()
    output = interpolator.GetOutput()

    #writer = vtkXMLUnstructuredGridWriter()
    #writer.SetInputData( output )
    #writer.SetFileName( os.path.join( os.path.dirname( args.outputMesh ), "validInterpolation.vtu" ) )
    #writer.Update()

    # Threshold to remove non-valid points:
    #thresh = vtkThreshold()
    #thresh.SetInputArrayToProcess(0, 0, 0, vtkDataObject.FIELD_ASSOCIATION_POINTS, "validInternalPoints");
    #thresh.ThresholdBetween( 0.5, 1.5 )
    #thresh.SetInputData( output )
    #thresh.Update()
    #outputThresh = thresh.GetOutput()
    #print("number of points:", outputThresh.GetNumberOfPoints())
    #print(outputThresh.GetPointData().GetNumberOfArrays())

    # Apply inverse transform:
    #tf.Inverse()
    #tfFilter = vtkTransformFilter()
    #tfFilter.SetTransform( tf )
    #tfFilter.SetInputData( output )
    #tfFilter.Update()
    #output = tfFilter.GetOutput()

    writer = vtkXMLUnstructuredGridWriter()
    writer.SetInputData( fieldInternal )
    writer.SetFileName( os.path.join( os.path.dirname( args.outputMesh ), "fieldInternal.vtu" ) )
    writer.Update()

    if args.reference is not None:


        # Load the input mesh:
        fileType = args.reference[-4:].lower()
        if fileType == ".stl":
            reader = vtkSTLReader()
            reader.SetFileName( args.reference )
            reader.Update()
            reference = reader.GetOutput()
        elif fileType == ".vtk":
            reader = vtkUnstructuredGridReader()
            reader.SetFileName( args.reference )
            reader.Update() 
            reference = reader.GetOutput()
        elif fileType == ".vtu":
            reader = vtkXMLUnstructuredGridReader()
            reader.SetFileName( args.reference )
            reader.Update() 
            reference = reader.GetOutput()
        elif fileType == ".vtp":
            reader = vtkXMLPolyDataReader()
            reader.SetFileName( args.reference )
            reader.Update() 
            reference = reader.GetOutput()
        elif fileType == ".txt":
            with open( args.mesh ) as f:
                pts = vtkPoints()
                verts = vtkCellArray()
                i = 0
                for l in f:
                    l = l.replace(',', " ")
                    x,y,z = l.split(" ", 3)
                    x = float(x)
                    y = float(y)
                    z = float(z)
                    pts.InsertNextPoint( x, y, z )
                    verts.InsertNextCell( 1, (i,) )
                    i = i+1
                reference = vtkPolyData()
                reference.SetPoints( pts )
                reference.SetVerts( verts )
     
        else:
            raise IOError("Reference mesh should be .vtk, .vtu, .vtp, .txt or .stl file")


        print("WARNING: A Reference mesh is given. This script assumes that there is exactly one point in the reference for each point in the input. It also assumes that their order in the files show their correspondence, i.e. the first point in the 'mesh' should correspond to the first point in 'reference'! If this is the case, continue safely. If not, please ignore the calculated metrics.")
    
        errs = []
        targetDists = []
        displs = []

        if output.GetNumberOfPoints() != reference.GetNumberOfPoints():
            raise IOError("FATAL: The points in the displaced mesh must correspond to the points in the reference mesh, but got {} and {} points.".format( output.GetNumberOfPoints(), reference.GetNumberOfPoints() ) )

        validInternalPoints = output.GetPointData().GetArray( "validInternalPoints" )
        displacement = output.GetPointData().GetArray( args.displacementFieldName )
        displacementErrorMesh = vtkDoubleArray()
        displacementErrorMesh.SetName( "displacementErrorMesh" )
        displacementErrorMesh.SetNumberOfComponents(3)
        displacementErrorMesh.SetNumberOfTuples( output.GetNumberOfPoints() )
        for i in range( output.GetNumberOfPoints() ):
            validity = validInternalPoints.GetTuple1(i)
            if validity > 0.5:
                p = output.GetPoint(i)
                p = numpy.asarray(p)
                r = reference.GetPoint(i)
                r = numpy.asarray(r)
                d = displacement.GetTuple3(i)
                d = numpy.asarray(d)
                if args.verbose:
                  print("Point", i)
                  print("\tp+d", p, "+", d)
                  print("\tref", r)
                
                p_d = p + d

                err = p_d-r

                errMag = numpy.linalg.norm( err )
                errs.append(errMag)

                targetDist = numpy.linalg.norm( p-r )
                targetDists.append(targetDist)

                displ = numpy.linalg.norm( d )
                displs.append( displ )
                
                displacementErrorMesh.SetTuple3( i, err[0], err[1], err[2] )

                #if isInternalPoint( output,i ):
                    #errsInternal.append( errMag )
                    #targetDistsInternal.append( targetDist )
                    #displsInternal.append( displ )

                if args.verbose:
                    print("\tDisplacement", targetDist)
                    print("\t(Error " + str(err) + ")")
            else:
                if args.verbose:
                    print( "Invalid point (external). Skipping." )

        output.GetPointData().AddArray( displacementErrorMesh )

        print("Stats for all points:")
        print( "\tMean target displacement:", sum(targetDists)/len(targetDists) )
        print( "\tMax target displacement:", max(targetDists) )
        print( "\tStd of target displacement:", statistics.stdev(targetDists) )
        print( "\tMean estimated displacement:", sum(displs)/len(displs) )
        print( "\tMean error in displacement:", sum(errs)/len(errs) )
        print( "\tMax error in displacement:", max(errs) )
        print( "\tStd of error in displacement:", statistics.stdev(errs) )
        print( "\tMedian error in displacement:", statistics.median(errs) )
        #print("Stats for internal points only:")
        #print( "\tMean target displacement:", sum(targetDistsInternal)/len(targetDistsInternal) )
        #print( "\tMax target displacement:", max(targetDistsInternal) )
        #print( "\tMean estimated displacement:", sum(displsInternal)/len(displsInternal) )
        #print( "\tMean error in displacement:", sum(errsInternal)/len(errsInternal) )
        #print( "\tMax error in displacement:", max(errsInternal) )
        #print( "\tMedian error in displacement:", statistics.median(errsInternal) )

    append = vtkAppendFilter()
    append.AddInputData( output )
    append.Update()
    output = append.GetOutput()

    writer = vtkXMLUnstructuredGridWriter()
    writer.SetInputData( output )
    writer.SetFileName( args.outputMesh )
    writer.Update()


