import os
import sys
import glob, shutil, subprocess
import yaml

# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s %s %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()

# Check if string is a valid path. If not, try to create it. If not, fail.
def path(string):
    if os.path.isdir(string):
        return string
    else:
        try:
            os.makedirs(string)
            return string
        except:
            raise NotADirectoryError(string)

# Check if string is a valid file. If not, fail.
def filepath(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)

def saveCurrentState( outputFolder, args ):
    result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE)
    sha = result.stdout.decode('utf-8')
    result = subprocess.run(['hostname'], stdout=subprocess.PIPE)
    hostname = result.stdout.decode('utf-8')

    if not os.path.exists( outputFolder ):
        os.makedirs( outputFolder )

    print("Saving output to: {}".format(outputFolder))

    runinfoFile = os.path.join( outputFolder, "RunInfo" )
    print("Saving run info to {}".format(runinfoFile) )
    with open( runinfoFile, "w" ) as f:
        f.write("Git commit:\t" + str(sha))
        f.write("Hostname:\t" + str(hostname))
        f.write("\nArguments:\n")
        for key,value in vars(args).items():
            f.write("\t" + key + "\t" + str(value) + "\n")



    # Make backup copy of data:
    for f in glob.glob("./*.py"):
        print("\tMaking backup copy of:", f)
        shutil.copy( f, args.outputFolder )

