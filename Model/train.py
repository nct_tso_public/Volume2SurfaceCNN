import argparse
from utils import *
from loss import Loss, SurfaceLoss
import traceback
import gc
import math

##################################################
## Handle input arguments

parser = argparse.ArgumentParser(description="Train model on deformation data.")
parser.add_argument("dataFolder", type=path, help="Folder(s) in which the samples lie")
parser.add_argument("--outputFolder", required=True, type=path, help="Folder in which to save the results and models")
parser.add_argument("--num", type=int, default=1000, help="Amount of data samples to load from [dataFolder]")
parser.add_argument("--batchSize", type=int, default=6, help="Batch size to use during training")
parser.add_argument("--epochs", type=int, default=100, help="Number of epochs to train")
parser.add_argument("--resume", action="store_true", help="Check for checkpoints in outputFolder and resume training from there")
parser.add_argument("--pretrained", type=path, help="Folder in which a pretrained model is stored")
parser.add_argument("--no_augmentation", action="store_true", help="Disable data augmentation")
args = parser.parse_args()

##################################################
## Make backup copies, write potentionally important info
saveCurrentState( args.outputFolder, args )

##################################################
# Set up logging with TensorboardX:
import tensorboardX
trainingLog = tensorboardX.SummaryWriter(os.path.join(args.outputFolder, "logs"), write_to_disk=True )
testLog = tensorboardX.SummaryWriter(os.path.join(args.outputFolder, "logs_test"), write_to_disk=True )

##################################################
## Set up training and testing data:
import data
import torch
import torch.nn as nn

useDataAugmentation = not args.no_augmentation

print("Loading data from:", args.dataFolder)
numTraining = int(0.95*args.num)        # Use 95 % of the data for training
numTest = int(0.05*args.num)            # Use 5 % of the data for validation
trainData = data.VoxelizedDataset( args.dataFolder, useDataAugmentation, startIndex=0, num=numTraining )
trainData.silent = True
testData = data.VoxelizedDataset( args.dataFolder, False, startIndex=numTraining, num=numTest )
testData.silent = True

trainData.setMaximumAllowedDisplacement( 0.2 )
trainData.setMinimumAllowedVisSurface( 0.1 )
trainData.prune()
testData.setMaximumAllowedDisplacement( 0.2 )
testData.setMinimumAllowedVisSurface( 0.1 )
testData.prune()

print("Training data loaded. ({} samples).".format(len(trainData)))
print("Test data loaded. ({} samples).".format(len(testData)))

trainDataLoader = torch.utils.data.DataLoader( trainData,
        batch_size=args.batchSize,
        shuffle=True,
        pin_memory=True,
        num_workers=2 )

testDataLoader = torch.utils.data.DataLoader( testData,
        batch_size=args.batchSize,
        shuffle=False,
        pin_memory=True,
        num_workers=2,
        drop_last=False )

##################################################
## Set up model:
import model
import checkpoint

checkpointPath = os.path.join( args.outputFolder, "checkpoints" )
if not os.path.exists( checkpointPath ):
    os.makedirs( checkpointPath )

print("Loading model")
model = model.Model().cuda()
model.init()
#x1 = torch.randn(1,1,64,64,64).cuda()
#x2 = torch.randn(1,1,64,64,64).cuda()
#trainingLog.add_graph( model, (x1,x2) )
import torch.optim


##################################################
## Set up training:
#optimizer = optimizer.Adam()

#learningRateMax = 7.5e-5
#learningRateMin = 1e-6
if not args.pretrained:
    learningRateMax = 1e-4
else:
    learningRateMax = 1e-6
print("Max Learning Rate:", learningRateMax)
learningRateStart = learningRateMax/2
#learningRateEnd = learningRateMax/1e4
#totalSteps = len(trainData)/args.batchSize*100
#totalSteps = len(trainData)/args.batchSize*100
#learningRateMin = 1e-4
#learningRateMin = learningRateMax/10
#learningRate = 5e-6
#learningRate = 2e-4

optimizer = torch.optim.AdamW( model.parameters(), lr = learningRateStart )

epochs = args.epochs

#import cyclicalLR
startEpoch = 0
#scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma = 1 )
#step_size = len(trainDataLoader)
#clr = cyclicalLR.cyclicalLR(step_size, min_lr=learningRateMin, max_lr=learningRateMax)
#scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, [clr])
#scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer, learningRateMin, learningRateMax, step_size_up=200, cycle_momentum=False)
scheduler = torch.optim.lr_scheduler.OneCycleLR( optimizer, learningRateMax,
        epochs=epochs+1,
        steps_per_epoch=int(len(trainData)/args.batchSize),
        div_factor = 2)


curStep = 0

##################################################
## Loss:

#surfaceLoss = SurfaceLoss( voxelsPerSide=64, size=0.3 )

##################################################
## Load from previous run:

if args.pretrained:
    _, _, _ = checkpoint.load( args.pretrained, model )
    print("Loaded pretrained model from:", args.pretrained)
elif args.resume:
    startEpoch, _, _ = checkpoint.load( checkpointPath, model, optimizer, scheduler )
    print("Resuming from checkpoint, epoch {}".format(startEpoch))
    startEpoch += 1

##################################################
## Training:
import test
from timeit import default_timer as timer

prevTime = timer()
avgTime = 0

#def updateVisibleAmount( visibleSurfaceAmount ):
#    global trainData
#    curTrainData = data.SubsetDataset( trainData, visibleSurfaceAmount )
#    print("len", len(curTrainData))
#    trainDataLoader = torch.utils.data.DataLoader( curTrainData,
#        batch_size=args.batchSize,
#        shuffle=True,
#        num_workers=8,
#        drop_last=True)
#    print("New visible surface amount: {} ({} samples)".format(visibleSurfaceAmount,
#        len(curTrainData)))

#visibleSurfaceAmount = 0.95 - 0/1000
#curTrainData, trainDataLoader = updateVisibleAmount( 0 )

print("Start training...")
for epoch in range( startEpoch, epochs ):

    model.train()
    #if epoch % 10 == 9:
        #visibleSurfaceAmount = 0.95 - epoch/1000
        #curTrainData, trainDataLoader = updateVisibleAmount( visibleSurfaceAmount )

    epochStartTime = timer()

    losses = []
    lossesS = []
    losses64 = []
    losses32 = []
    losses16 = []
    losses8 = []

    for i_batch, d in enumerate(trainDataLoader):

        #printProgressBar( i_batch+1, len(trainDataLoader), "Epoch {}:".format( epoch ), decimals=2 )
        try:
            optimizer.zero_grad()

            # Prepare input data:
            preoperative = d["preoperative"].cuda( non_blocking=True )
            intraoperative = d["intraoperative"].cuda( non_blocking=True )

            # Set up loss mask:
            #mask64 = (preoperative <= 0).float()
            #mask32 = nn.functional.interpolate( mask64, size=32 )
            #mask16 = nn.functional.interpolate( mask64, size=16 )
            #mask8 = nn.functional.interpolate( mask64, size=8 )

            # Prepare target data:
            target64 = d["targetDisplacement"].cuda( non_blocking=True )
            target32 = nn.functional.interpolate( target64, size=32 )
            target16 = nn.functional.interpolate( target64, size=16 )
            target8 = nn.functional.interpolate( target64, size=8 )
            preop32 = nn.functional.interpolate( preoperative, size=32 )
            preop16 = nn.functional.interpolate( preoperative, size=16 )
            preop8 = nn.functional.interpolate( preoperative, size=8 )

            # Run model:
            out64, out32, out16, out8 = model( preoperative, intraoperative )

            # Loss calculation:
            l64 = Loss( out64, target64, torch.abs(preoperative) )
            l32 = Loss( out32, target32, torch.abs(preop32) )
            l16 = Loss( out16, target16, torch.abs(preop16) )
            l8 = Loss( out8, target8, torch.abs(preop8) )
            #lS = surfaceLoss.calc( preoperative, intraoperative, out64 )
            #lS = torch.zeros( 1 ).cuda()
            loss = 10*l64 + l32 + l16 + l8# + lS
            #loss = l64

            # Update:
            loss.backward()
            optimizer.step()
            scheduler.step()    # Modify Learning Rate
            #lr = oneCycleLRUpdate()
            curStep = curStep + 1 
            trainingLog.add_scalar("lr", optimizer.param_groups[0]["lr"], curStep )
            #print(lr)

            losses.append( loss.item() )
            losses64.append( l64.item() )
            losses32.append( l32.item() )
            losses16.append( l16.item() )
            losses8.append( l8.item() )
            #lossesS.append( lS.item() )

            printProgressBar( i_batch+1, len(trainDataLoader), prefix="Epoch: {}".format(epoch), suffix="(Avg. time: {:.2f} ms)".format( 1000*avgTime ), decimals=2 )
            # Explicitly clean to avoid cuda memory errors: 
            del preoperative, intraoperative
            del target64, target32, target16, target8
            del out64, out32, out16, out8
            del l64, l32, l16, l8#, lS
            del loss
            del d

        except Exception as err:
            print(traceback.format_exc(), err)
            print( "Error: Failed to process batch:", i_batch )

        if i_batch % 20 == 19:
            curTime = timer()
            avgTime = (curTime-prevTime)/(20*args.batchSize)
            prevTime = curTime

        #gc.collect()        

    if len(losses) == 0:
        losses.append(0)
    if len(losses64) == 0:
        losses64.append(0)
    if len(losses32) == 0:
        losses32.append(0)
    if len(losses16) == 0:
        losses16.append(0)
    if len(losses8) == 0:
        losses8.append(0)
    if len(lossesS) == 0:
        lossesS.append(0)
    trainingLog.add_scalar("loss", sum(losses)/len(losses), epoch )
    trainingLog.add_scalar("l64", sum(losses64)/len(losses64), epoch )
    trainingLog.add_scalar("l32", sum(losses32)/len(losses32), epoch )
    trainingLog.add_scalar("l16", sum(losses16)/len(losses16), epoch )
    trainingLog.add_scalar("l8", sum(losses8)/len(losses8), epoch )
    #trainingLog.add_scalar("lS", sum(lossesS)/len(lossesS), epoch )

    print("Test...")
    stats = test.testModelBatched( model, testDataLoader, printErrors=True )
    if len(stats) > 0:
        losses = [d["loss"] for d in stats]
        avgTestLoss = sum(losses)/len(losses)

        meanDisplacementErrs = [d["meanDisplacementErr"] for d in stats]
        meanDisplacementErr = sum(meanDisplacementErrs)/len(meanDisplacementErrs)

        testLog.add_scalar("loss", avgTestLoss, epoch )
        testLog.add_scalar("meanDisplacementErr", meanDisplacementErr, epoch )
        print("Test loss: {:.2e} Mean displacement error: {:.4f} m".format(avgTestLoss, meanDisplacementErr))
    else:
        print("Warning: No validation samples!")

    epochTime = timer() - epochStartTime
    print("Epoch time: {:.2f} s".format( epochTime ))

    #print( epoch, " Loss: {:e} {:e} {:e} {:e} Vis: {:e} \tTotal: {:e}".format(
    #                avgTrainErrs[0], avgTrainErrs[1], avgTrainErrs[2], avgTrainErrs[3], avgTrainErrs[4],
    #                avgTrainErrs[5] ),
    #                "\n\tTest: {:e} {:e} {:e} {:e} Vis: {:e} \tTotal: {:e}".format(
    #                avgTestErrs[0], avgTestErrs[1], avgTestErrs[2], avgTestErrs[3], avgTestErrs[4],
    #                avgTestErrs[5] ),
    #                "\n\tLearning Rate: {:.2e}, Time: {:2e}".format( optimizer.param_groups[0]['lr'], dt ) )

    #plotErrors( avgTrainErrs, avgTestErrs )

    #plt.clf()
    #plt.hist( trainData.drawn, int(len(trainData)/8) )
    #plt.grid(True)
    #plt.savefig( outPath + "/drawing" )

    #trainingLog.add_scalar("lr", optimizer.param_groups[0], epoch )

    #if epoch % 5 == 0:
    #torch.save(net.state_dict(), outPath + "/model" )
    if epoch % 5 == 0 or epoch == startEpoch + epochs - 1:
        print("Saving checkpoint")
        checkpoint.save( checkpointPath, epoch, model, optimizer, scheduler )
    #print(scheduler.max_lrs)
    #maxLR = learningRateMin + (learningRateMax-learningRateMin)*math.exp(-1e-2*epoch)
    #maxLR = learningRateMin + (learningRateMax-learningRateMin)*max(1-epoch/300,0)
    #print("Learning rates (min, max, curMax):", learningRateMin, learningRateMax, maxLR )
    #for i in range(len(scheduler.max_lrs)):
        #scheduler.max_lrs[i] = maxLR

    #trainingLog.add_scalar("maxLR", maxLR, epoch )
    #trainingLog.add_scalar("lr", scheduler.get_lr()[0], epoch )

##################################################
## Clean up:
trainingLog.close()
testLog.close()


