import numpy
import statistics
from utils import *
import matplotlib.pyplot as plt

def calcStats( stats ):
    losses = [d["loss"] for d in stats]
    visAmounts = [d["visibleAmount"] for d in stats]
    meanDisplacementErrs = [d["meanDisplacementErr"] for d in stats]
    meanTargetDisplacements = [d["meanTargetDisplacement"] for d in stats]
    meanDisplacements = [d["meanDisplacement"] for d in stats]
    times = [d["time"] for d in stats]
    if len(times) > 0:
      medianTime = statistics.median(times)
    else:
      medianTime = -1

    print("Mean loss:", sum(losses)/len(losses))
    print("Mean target displacement:", sum(meanTargetDisplacements)/len(meanTargetDisplacements))
    print("Mean displacement error:", sum(meanDisplacementErrs)/len(meanDisplacementErrs) )
    print("Median mean displacement error:", statistics.median(meanDisplacementErrs) )
    print("Median time: {:.2f} (~ {:.2f} FPS)".format( medianTime, 1e3/medianTime ) )

    # Find samples with largest error in displacement:
    sortedStats = sorted(stats, key=lambda i: i["meanDisplacementErr"])
    worst = sortedStats[-10:]
    print("Samples with worst mean error:")
    for i in range(len(worst)-1,-1,-1):
        w = worst[i]
        print( "\t{}: {:.2e}".format(w["index"], w["meanDisplacementErr"] ) )

    # Find samples with smallest visible amount:
    sortedStats = sorted(stats, key=lambda i: -i["visibleAmount"])
    lowest = sortedStats[-10:]
    print("Samples with lowest visible amount:")
    for i in range(len(lowest)-1,-1,-1):
        w = lowest[i]
        print( "\t{}: {:.2f}% (mean err: {:.2e})".format(w["index"], 100*w["visibleAmount"], w["meanDisplacementErr"]) )

    # Find samples with largest displacement to visible amount ratio:
    for i in range(len(stats)):
        stats[i]["ratio"] = stats[i]["maxTargetDisplacement"]/stats[i]["visibleAmount"]
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with largest max target displacement to visible surface amount ratio")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2f})".format(w["index"], w["ratio"], w["maxTargetDisplacement"], w["visibleAmount"] ) )



    # Find samples with largest ratio of error to mean target displacemnt:
    for i in range(len(stats)):
        if stats[i]["meanTargetDisplacement"] > 0.01:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]/stats[i]["meanTargetDisplacement"]
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with worst mean error to mean target displacement ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["meanTargetDisplacement"] ) )

    # Find samples with largest ratio of error to max target displacemnt:
    for i in range(len(stats)):
        if stats[i]["maxTargetDisplacement"] > 0.01:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]/stats[i]["maxTargetDisplacement"]
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with worst mean error to max target displacement ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["maxTargetDisplacement"] ) )


    # Find samples with largest ratio of max error to mean error:
    for i in range(len(stats)):
        if stats[i]["meanDisplacementErr"] > 0.002:
            if stats[i]["maxDisplacementErr"] > 0.01:
                stats[i]["ratio"] = stats[i]["maxDisplacementErr"]/stats[i]["meanDisplacementErr"]
            else:
                stats[i]["ratio"] = -1
        else:
            stats[i]["ratio"] = -1
    sortedStats = sorted(stats, key=lambda i: i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with largest max error to mean error ratios:")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e})".format(w["index"], w["ratio"], w["maxDisplacementErr"], w["meanDisplacementErr"] ) )

    # Find samples with low mean error and low vis. surface but high mean target displacement:
    for i in range(len(stats)):
        if stats[i]["meanTargetDisplacement"] > 0.005:
            stats[i]["ratio"] = stats[i]["meanDisplacementErr"]*stats[i]["visibleAmount"]/stats[i]["meanTargetDisplacement"]
        else:
            stats[i]["ratio"] = 1
    sortedStats = sorted(stats, key=lambda i: -i["ratio"])
    worstRatios = sortedStats[-10:]
    print("Samples with lowest mean error to mean target displacement")
    for i in range(len(worstRatios)-1,-1,-1):
        w = worstRatios[i]
        print( "\t{}: {:.2e} ({:.2e}/{:.2e}, {:.2f})".format(w["index"], w["ratio"], w["meanDisplacementErr"], w["meanTargetDisplacement"], w["visibleAmount"]) )




#plt.savefig( os.path.join( args.outputFolder, "ErrsOverVisAmounts.png"), bbox_inches='tight')

def generatePlots( stats, outputFolder ):
    losses = [d["loss"] for d in stats]
    visAmounts = [d["visibleAmount"]*100 for d in stats]
    meanDisplacementErrs = [d["meanDisplacementErr"]*1000 for d in stats]
    maxDisplacementErrs = [d["maxDisplacementErr"]*1000 for d in stats]
    meanTargetDisplacements = [d["meanTargetDisplacement"]*1000 for d in stats]
    maxTargetDisplacements = [d["maxTargetDisplacement"]*1000 for d in stats]
    meanDisplacements = [d["meanDisplacement"]*1000 for d in stats]
    maxDisplacements = [d["maxDisplacement"]*1000 for d in stats]
    markerScale = [(s/10+2)**2 for s in visAmounts]
    #cmap = plt.cm.get_cmap("autumn")
    cmap = plt.cm.get_cmap("plasma")

    bins = []
    binContent = []
    numBins = 10
    binSize = 1/(numBins)
    for i in range(0,numBins):
        v = binSize*(i+0.5)
        bins.append(v*100)
        binContent.append( [] )

    for d in stats:
        i = int(d["visibleAmount"]*(numBins))
        if i > numBins-1:
            i = numBins - 1
        binContent[i].append( d["meanDisplacementErr"]*1000 )

    avgBinContent = []
    nonEmptyBins = []
    for i in range(0,numBins):
        if len(binContent[i]) > 0:
            nonEmptyBins.append(bins[i])
            avgBinContent.append( sum(binContent[i])/len(binContent[i]) )

    import matplotlib.lines as mlines
    s = numpy.sqrt(min(markerScale))+3
    minValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(0.), marker='o',
            markersize=s, label=round(min(visAmounts),1))

    meanMarkerSize = (max(markerScale)-min(markerScale))/2
    meanVisAmount = (max(visAmounts)-min(visAmounts))/2
    s = numpy.sqrt(meanMarkerSize)*(32/25)
    meanValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(0.5), marker='o',
            markersize=s, label=round(meanVisAmount,1))

    s = numpy.sqrt(max(markerScale))*(32/25)
    maxValLegend = mlines.Line2D([], [], color="w", markerfacecolor=cmap(1.), marker='o',
            markersize=s, label=round(max(visAmounts),1))


    plt.clf()
    plt.figure(figsize=(8,3))
    plt.plot(nonEmptyBins,avgBinContent, "*-" )
    plt.xlabel( "Visible amount (%)" )
    plt.ylabel( "Mean displacement error (mm)" )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverVisAmounts.eps"), bbox_inches='tight')

    plt.clf()
    plt.figure(figsize=(8,8))
    plt.scatter( visAmounts, meanDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Visible amount (%)" )
    plt.ylabel( "Mean displacement error (mm)" )
    plt.axis("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xlim( 0, 100 )
    plt.xticks( [i*10 for i in range(10)] )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverVisAmountsScatter.eps"), bbox_inches='tight')

    plt.clf()
    plt.figure(figsize=(8,2.5))
    plt.scatter(meanTargetDisplacements, meanDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Mean target displacement (mm)" )
    plt.ylabel( "Mean displ. error (mm)" )
    #plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xlim( 0, 70 )
    plt.ylim( 0, 60 )
    #plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(7)] )
    plt.savefig( os.path.join( outputFolder, "MeanErrsOverMeanTargetDisplacement.eps"), bbox_inches='tight')

    plt.clf()
    plt.scatter(maxTargetDisplacements, maxDisplacementErrs, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Max target displacement (mm)" )
    plt.ylabel( "Max displacement error (mm)" )
    plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(8)] )
    plt.savefig( os.path.join( outputFolder, "MaxErrsOverMaxTargetDisplacement.eps"), bbox_inches='tight')


    plt.clf()
    #fig = plt.figure(figsize=(7,6))

    plt.scatter( meanTargetDisplacements, meanDisplacements, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Mean target displacement (mm)" )
    plt.ylabel( "Mean displacement (mm)" )
    #plt.gca().set_adjustable("box")
    #plt.gca().axis("square")
    plt.gca().set_aspect("equal")
    plt.grid(True)
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(10)] )
    plt.savefig( os.path.join( outputFolder, "MeanDisplacements.eps"), bbox_inches='tight')
    #print( max(meanTargetDisplacements), max(meanDisplacements))

    plt.clf()
    scat = plt.scatter( maxTargetDisplacements, maxDisplacements, s=markerScale, c=visAmounts, cmap=cmap )
    plt.xlabel( "Max target displacement (mm)" )
    plt.ylabel( "Max displacement (mm)" )
    plt.xlim( 0, 100 )
    plt.ylim( 0, 100 )
    plt.xticks( [i*10 for i in range(10)] )
    plt.yticks( [i*10 for i in range(10)] )
    plt.legend(handles=[maxValLegend, meanValLegend, minValLegend], title="Vis. Surface (%)")
    plt.axis("equal")
    plt.grid(True)
    plt.legend()
    #handles, labels = scat.legend_elements(prop="sizes", alpha=0.6)
    #legend2 = plt.gca().legend(handles, labels, loc="upper right", title="Sizes")
    plt.savefig( os.path.join( outputFolder, "MaxDisplacements.eps"), bbox_inches='tight')

if __name__ == "__main__":
    import argparse
    import pickle

    parser = argparse.ArgumentParser(description="Train model on deformation data.")
    parser.add_argument("folder", type=path, help="Folder in which to save the plots. Must contain a 'results.pkl' as generated by apply.py")
    args = parser.parse_args()


    # Load results previously calculated by apply.py:
    filename = os.path.join( args.folder, "results.pkl" )
    if not os.path.exists( filename ):
        raise IOError("Could not find 'results.pkl' in {}!".format( args.folder ))
    
    stats = pickle.load( open( filename, 'rb' ) )

    calcStats( stats )
    generatePlots( stats, args.folder )

