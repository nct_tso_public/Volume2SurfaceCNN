import argparse
from utils import *
from loss import Loss
import traceback
from matplotlib import pyplot as plt
import gc

##################################################
## Handle input arguments

parser = argparse.ArgumentParser(description="Test for good learning rate.")
parser.add_argument("dataFolder", type=path, help="Folder in which the samples lie")
parser.add_argument("--outputFolder", required=True, type=path, help="Folder in which to save the results")
parser.add_argument("--num", type=int, default=1000, help="Amount of data samples to load from [dataFolder]")
parser.add_argument("--batchSize", type=int, default=10, help="Batch size to use during training")
args = parser.parse_args()

##################################################
## Make backup copies, write potentionally important info
import glob, shutil
import tensorboardX

# Make backup copy of data:
for f in glob.glob("./*.py"):
    print("Making backup copy of:", f)
    shutil.copy( f, args.outputFolder )

# Set up TensorboardX:
trainingLog = tensorboardX.SummaryWriter(os.path.join(args.outputFolder, "logs"), write_to_disk=True )
#testLog = tensorboardX.SummaryWriter(os.path.join(args.outputFolder, "logs_test"), write_to_disk=True )

##################################################
## Set up training and testing data:
import data
import torch
import torch.nn as nn

numTraining = int(0.5*args.num)
numTest = int(0.5*args.num)
trainData = data.VoxelizedDataset( args.dataFolder, True, startIndex=0, num=numTraining )
print("Training data loaded. ({} samples).".format(len(trainData)))
#testData = data.VoxelizedDataset( args.dataFolder, False, startIndex=numTraining, num=numTest )
#print("Test data loaded. ({} samples).".format(len(testData)))

#trainData.setMaximumAllowedDisplacement( 0.1 )
#print("Training data cleaned. ({} samples).".format(len(trainData)))
#testData.setMaximumAllowedDisplacement( 0.1 )
#print("Test data cleaned. ({} samples).".format(len(testData)))

trainDataLoader = torch.utils.data.DataLoader( trainData,
        batch_size=args.batchSize,
        shuffle=True,
        num_workers=8 )

#testDataLoader = torch.utils.data.DataLoader( testData,
#        batch_size=args.batchSize,
#        shuffle=False,
#        num_workers=8 )

##################################################
## Set up model:
import model
print("Loading model")
model = model.Model().cuda()

#import test
import torch.optim

##################################################
## Set up training:
#optimizer = optimizer.Adam()

initLR = 1e-7
maxLR = 2
n = len(trainDataLoader)
q = (maxLR/initLR)**(1/n)

optimizer = torch.optim.AdamW( model.parameters(), lr = initLR )

##################################################
## Test for best learning rate:

model.train()
#if epoch % 10 == 9:
    #visibleSurfaceAmount = 0.95 - epoch/1000
    #curTrainData, trainDataLoader = updateVisibleAmount( visibleSurfaceAmount )

losses = []
lrs = []
for i_batch, d in enumerate(trainDataLoader):
    #lr += min(lr, 2e-2)
    lr = initLR*q**i_batch
    print(str(i_batch) + "/" + str(len(trainDataLoader)), "Learning rate:", lr)
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

    #printProgressBar( i_batch+1, len(trainDataLoader), "Epoch {}:".format( epoch ), decimals=2 )
    try:
        optimizer.zero_grad()

        # Prepare input data:
        preoperative = d["preoperative"].cuda()
        intraoperative = d["intraoperative"].cuda()

        # Set up loss mask:
        mask64 = (preoperative <= 0).float()
        mask32 = nn.functional.interpolate( mask64, size=32 )
        mask16 = nn.functional.interpolate( mask64, size=16 )
        mask8 = nn.functional.interpolate( mask64, size=8 )

        # Prepare target data:
        target64 = d["targetDisplacement"].cuda()
        target32 = nn.functional.interpolate( target64, size=32 )
        target16 = nn.functional.interpolate( target64, size=16 )
        target8 = nn.functional.interpolate( target64, size=8 )

        # Run model:
        out64, out32, out16, out8 = model( preoperative, intraoperative )

        # Loss calculation:
        l64 = Loss( out64*mask64, target64*mask64 )
        l32 = Loss( out32*mask32, target32*mask32 )
        l16 = Loss( out16*mask16, target16*mask16 )
        l8 = Loss( out8*mask8, target8*mask8 )
        loss = l64 + l32 + l16 + l8

        # Update:
        loss.backward()
        optimizer.step()

        #losses.append( loss.item() )
        trainingLog.add_scalar("loss", loss.item(), i_batch )
        trainingLog.add_scalar("lr", lr, i_batch )
        #trainingLog.add_scalar("loss_over_lr", loss.item(), lr )

        losses.append(loss.item())
        lrs.append(lr)
        if i_batch % 20 == 0:
            plt.clf()
            plt.plot( lrs, losses, '*' )
            trainingLog.add_figure("loss over lr", plt.gcf(),i_batch)

        trainingLog.close()
        print(i_batch, "Loss", loss.item(), "lr", lr)

        #printProgressBar( i_batch+1, len(trainDataLoader), suffix="(Sample loss: {:.2e})".format(loss.item()), decimals=2 )
        # Explicitly clean to avoid cuda memory errors: 
        del preoperative, intraoperative
        del target64, target32, target16, target8
        del out64, out32, out16, out8
        del l64, l32, l16, l8
        del loss

    except Exception as err:
        print(traceback.format_exc(), err)
        print( "Error: Failed to process batch:", i_batch )

    gc.collect()

