import torch
import shutil
import os

def save( path, epoch, model, optimizer, scheduler=None, trainErrs=[], testErrs=[] ):
    # Backup copy of a file if it already exists:
    # Note: This only saves the last epoch! Previous Epochs will be overwritten.
    # This is just a safety measure in case something goes wrong during writing.
    try:
        shutil.move( path + "/checkpoint.tar", path + "/checkpoint_prevEpoch.tar" )
    except:
        print("Could not make a backup copy of checkpoint.tar, maybe this is the first epoch?")

    filename = path + "/checkpoint.tar"
    state = {
            "model": model.state_dict(),
            "optimizer": optimizer.state_dict(),
            "epoch": epoch,
            "trainErrs": trainErrs,
            "testErrs": testErrs,
            }
    if scheduler is not None:
        state["scheduler"] = scheduler.state_dict()
    torch.save(state, filename)

def load( path, model, optimizer=None, scheduler=None ):
    filename = path + "/checkpoint.tar"
    if not os.path.exists(filename):
        raise OSError("checkpoint.tar could not be found.")
    checkpoint = torch.load(filename)
    model.load_state_dict(checkpoint["model"])
    if optimizer is not None:
        if "optimizer" in checkpoint:
            optimizer.load_state_dict(checkpoint["optimizer"])
    if scheduler is not None:
        if "scheduler" in checkpoint:
            scheduler.load_state_dict(checkpoint["scheduler"])
    trainErrs = checkpoint["trainErrs"]
    testErrs = checkpoint["testErrs"]
    return checkpoint["epoch"] + 1, trainErrs, testErrs
