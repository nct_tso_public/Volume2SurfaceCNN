import torch
import math
import data
from utils import *
#from pytorch_memlab import profile, profile_every, MemReporter
import gc

MSE = torch.nn.MSELoss()
def Loss( out, target, weights=None ):
    #return MSE(out, target)
    if weights is None:
        return torch.mean(torch.abs(out - target))  #MAE
    else:
        eps = 1e-2
        w = 1/(weights+eps)
        return torch.mean(torch.abs(out - target)*w)


class SurfaceLoss():

    def __init__(self, voxelsPerSide=64, size=0.3):

        voxelSize = size/voxelsPerSide

        self.voxelDiag = math.sqrt(voxelSize**2 + voxelSize**2)/2

        self.positions = torch.zeros( 3, voxelsPerSide, voxelsPerSide, voxelsPerSide )
        for x in range(0,voxelsPerSide):
            self.positions[0,:,:,x] = x
        for y in range(0,voxelsPerSide):
            self.positions[1,:,y,:] = y
        for z in range(0,voxelsPerSide):
            self.positions[2,z,:,:] = z

        self.positions *= voxelSize

        self.positions = self.positions.cuda()

        self.voxelsPerSide = voxelsPerSide

    # Calculate (in voxels) the minimal distance from the intraoperative surface to the
    # (displaced) preoperative surface:
    #@profile_every(1)
    def calc( self, preop, intraop, displacement ):

        #displacement = displacement.transpose( 1,0 )

        batchSize = displacement.shape[0]

        meanDists = torch.zeros( batchSize ).cuda()

        for b in range(batchSize):
            #print("batch:", b)
            displ = displacement[b]

            # Find voxels belonging to preoperative surface:
            p = preop[b].squeeze()
            p_ind = (p > -self.voxelDiag)*(p < 0)

            # Find voxels belonging to intraoperative surface:
            i = intraop[b].squeeze()
            i_ind = i < self.voxelDiag

            # Extract preoperative surface voxels:
            p_surf = self.positions[:,p_ind]
            # Extract intraoperative surface voxels:
            i_surf = self.positions[:,i_ind]          # intraoperative surface
            
            # Extract estimated displacement of the preoperative surface and apply it to the surface:
            displ_surf = displ[:,p_ind]
            p_surf += displ_surf      # displaced preoperative surface

            # Prepare for broadcasting:
            i_surf = i_surf.permute( 1,0 )
            p_surf = p_surf.permute( 1,0 )
            p_surf = p_surf.unsqueeze(1)     # add singleton dimension for broadcasting later:
            #print("preoperative surface points:", p_surf.shape[0])
            #print("intraoperative surface points:", i_surf.shape[0])

            # Find Eucledian distance:
            diff = i_surf - p_surf
            dists = torch.norm( diff, dim=2 )
            # For every intraoperative point, find minimum distance to preoperative points:
            minDists, _ = torch.min( dists, dim=0 )
            meanDists[b] = torch.mean(minDists)

            del minDists, diff, dists, i_surf, p_surf, displ_surf, i_ind, p_ind, displ, i, p

        mean = torch.mean(meanDists)

        return mean

#    def calc( self, preop, intraop, displacement ):
#
#        #displacement = displacement.transpose( 1,0 )
#        displacement.transpose_( 1,0 )
#
#        preopSurfaceIndex = (preop.squeeze() > -self.voxelDiag)*(preop.squeeze() < 0)
#        preopSurfaceIndex = preopSurfaceIndex.view( self.batchSize, self.voxelsPerSide, self.voxelsPerSide, self.voxelsPerSide )
#        preopSurface = self.positions[:,preopSurfaceIndex]
#        surfaceDisplacement = displacement[:,preopSurfaceIndex]
#
#        preopSurfaceDisplaced = preopSurface + surfaceDisplacement
#
#        intraopSurfaceIndex = intraop.squeeze() < self.voxelDiag
#        intraopSurfaceIndex = intraopSurfaceIndex.view( self.batchSize, self.voxelsPerSide, self.voxelsPerSide, self.voxelsPerSide )
#        intraopSurface = self.positions[:,intraopSurfaceIndex]
#
#        preopSurfaceDisplaced.transpose_( 1, 0 )
#        intraopSurface.transpose_( 1, 0 )
#
#        #intraopSurface = intraopSurface.view( intraopSurface.shape[0], 1, 3 )
#
#        #dists = torch.sqrt(torch.sum((preopSurface - reshaped)**2,dim=2))
#        #print(preopSurfaceDisplaced.shape, intraopSurface.shape)
#        #diff = preopSurfaceDisplaced - intraopSurface
#        #dists = torch.norm(diff,dim=2)
#
#        minDists = torch.zeros( intraopSurface.shape[0] ).cuda()
#        # Iterate over all intraoperative surface points:
#        for intraOpPoint in range(0, intraopSurface.shape[0] ):
#            # For every point, find the distance to all preoperative surface points:
#            diffs = intraopSurface[intraOpPoint,:] - preopSurfaceDisplaced 
#            dists = torch.norm( diffs, dim=1 )
#            minDists[intraOpPoint] = torch.min( dists )
#            del diffs, dists
#
#        #minDists,_ = torch.min( dists, dim=1 )
#        mean = torch.mean(minDists)
#
#        #reporter = MemReporter()
#        #reporter.report()
#
#        #gc.collect()
#
#        return mean
#


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Test surface distance loss")
    parser.add_argument("folder", type=path, help="Folder in which the samples lie")
    args = parser.parse_args()

    batchSize = 4

    dataset = data.VoxelizedDataset( args.folder, False, 0, 100 )
    dataLoader = torch.utils.data.DataLoader( dataset,
            batch_size=batchSize,
            shuffle=False, drop_last=True)

    dist = SurfaceLoss()
    #preoperative = torch.randn( 1, 64, 64, 64 ).cuda()
    #intraoperative = torch.randn( 1, 64, 64, 64 ).cuda()
    #displacement = torch.randn( 3, 64, 64, 64 ).cuda()

    for i, d in enumerate(dataLoader):
        preoperative = d["preoperative"].cuda()
        intraoperative = d["intraoperative"].cuda()
        displacement = d["targetDisplacement"].cuda()
        d0 = dist.calc( preoperative, intraoperative, displacement )
        d1 = dist.calc( preoperative, intraoperative, torch.zeros_like(displacement) )

        print( i, d0.item(), d1.item() )
        print(torch.mean( torch.norm( d["targetDisplacement"], dim=1 ) ).item() )


