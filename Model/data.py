import torch
from torch.utils.data import Dataset
import numpy
import vtk
from vtk.util import numpy_support
import random
import os
import traceback
import json
from utils import *

printedNoMetaDataWarning = False

class InvalidSampleError( Exception ):
    def __init__(self, msg):
        super().__init__( msg )
#def flipTensor( x, dim ):
    #xsize = x.size()
    #dim = x.dim() + dim if dim < 0 else dim
    #x = x.view(-1, *xsize[dim:])
    #x = x.view(x.size(0), x.size(1), -1)[:, getattr(torch.arange(x.size(1)-1, -1, -1), ('cpu','cuda')[x.is_cuda])().long(), :]
    #return x.view(xsize)

def flipSample( f, dims ):
    gridSize = f["intraoperative"].size(1)
    flippedInd = numpy.flip(numpy.arange(0,gridSize), axis=0)  # Flipped indices
    flippedInd = torch.LongTensor(flippedInd.copy())

    for dim in dims:
        flipDim = (2-dim)   # Account for dimensions being in order D,H,W

        # Flip data along an axis:
        f["intraoperative"] = torch.index_select( f["intraoperative"], flipDim+1, flippedInd )
        f["preoperative"] = torch.index_select( f["preoperative"], flipDim+1, flippedInd )
        f["targetDisplacement"] = torch.index_select( f["targetDisplacement"], flipDim+1, flippedInd )

        # Flip sign of the vector component of displacement along the flipped axis:
        f["targetDisplacement"][dim,:,:,:] = -f["targetDisplacement"][dim,:,:,:]
    return f

def saveSample( inPath, outPath, arraysToAppend ):

    # Read input:
    reader = vtk.vtkXMLStructuredGridReader()
    reader.SetFileName( inPath )
    reader.Update()
    voxels = reader.GetOutput()

    for name, tensor in arraysToAppend.items():
        if type(tensor) == torch.Tensor:
            np = tensor.detach().cpu().numpy()
            np = numpy.transpose( np, (1,2,3,0) )
            gridSize = np.shape[0]
            components = np.shape[3]
            np = numpy.reshape( np, (gridSize**3, components) )
            arr = numpy_support.numpy_to_vtk( np )
            arr.SetName( name )
            if voxels.GetPointData().HasArray(name):
                voxels.GetPointData().RemoveArray(name)
                print("Warning: Overwriting array {} (original file will stay untouched)".format(name))
            voxels.GetPointData().AddArray(arr)

    folder = os.path.dirname( outPath )
    if not os.path.exists( folder ):
        os.makedirs( folder )
    writer = vtk.vtkXMLStructuredGridWriter()
    writer.SetFileName( outPath )
    writer.SetInputData( voxels )
    writer.Update()

############################################################
## Basic dataset.
## Loads voxelized data and optionally augments it before returning.

class VoxelizedDataset( Dataset ):

    def __init__( self, path, augment, startIndex=0, num=10000, gridSize=64 ):
        self.path = path
        self.augment = augment
        self.gridSize = gridSize
        
        # Go through the path and try to find valid samples:
        self.validSamples = []
        #self.visibleAmounts = []
        self.invalidSamples = []

        self.maxAllowedDisplacement = 99999
        self.minAllowedVisSurface = -1

        self.retryOnLoadingFailure = True

        self.silent = True

        self.headerInfo = None

        self.createNullSample()

        headerInfoFile = os.path.join( self.path, "info.json" )
        if os.path.exists( headerInfoFile ):
            self.headerInfo = json.load( open( headerInfoFile ) )
            for i in range( startIndex, num + startIndex ):
                if "{:05d}".format(i) in self.headerInfo.keys():
                    self.validSamples.append(i)
        else:
            # If the header info file does not exists, check which files exist and assume every
            # file that exists is valid:
            for i in range( startIndex, num + startIndex ):

                printProgressBar( i+1-startIndex, num, "Checking Data", decimals=2 )
                try:
                    filenameInput = os.path.join( path, "{:05d}".format(i), "voxelized.vts" )

                    if not os.path.isfile(filenameInput):
                        raise IOError("Could not find file {}".format(filenameInput))

                    self.validSamples.append(i)

                except KeyboardInterrupt as err:    # If a keyboard interrupt was caught, pass it on
                    print("KeyboardInterrupt")
                    raise( err )
                except Exception as err:
                    #print("\n",traceback.format_exc())
                    #print(err)
                    pass


    def __len__(self):
        return len( self.validSamples )

    def setMaximumAllowedDisplacement( self, maxAllowedDisplacement=0.1 ):

        self.maxAllowedDisplacement = maxAllowedDisplacement

    def setMinimumAllowedVisSurface( self, minAllowedVisSurface=0.1 ):

        self.minAllowedVisSurface = minAllowedVisSurface

    # Remove all samples which have too high of a displacement:
    def prune( self ):

        if not self.headerInfo:
            print("WARNING: Cannot prune dataset. Create header info file with 'generateHeaderInfo.py' first!")
            return

        self.headerInfo

        newValidSamples = [] 
        numCleaned = 0
        for i in range( len(self.validSamples) ):
            preFix = "Pruning data ({})".format(numCleaned)
            printProgressBar( i+1, len(self.validSamples), preFix, decimals=2 )
           
            index = self.validSamples[i]
            sampleID = "{:05d}".format( index )
            sample = self.headerInfo[sampleID]
            if "displacement_max" in sample and sample["displacement_max"] > self.maxAllowedDisplacement:
                numCleaned += 1
                continue
            if "visibleSurfaceAmount" in sample and sample["visibleSurfaceAmount"] < self.minAllowedVisSurface:
                numCleaned += 1
                continue
            newValidSamples.append( index )

        self.validSamples = newValidSamples

    def __getitem__( self, i ):

        try:
            index = self.validSamples[i]
            sample = self.loadSample( self.path, index, gridSize=self.gridSize )
        
            if "displacementMax" in sample: 
                if sample["displacementMax"] > self.maxAllowedDisplacement:
                    raise InvalidSampleError("Maximum displacement too large")

            if self.augment:
                axis = random.randint( 0, 7 )
                if axis == 0:
                    sample = flipSample( sample, [0] )
                if axis == 1:
                    sample = flipSample( sample, [1] )
                if axis == 2:
                    sample = flipSample( sample, [2] )
                if axis == 3:
                    sample = flipSample( sample, [0,1] )
                if axis == 4:
                    sample = flipSample( sample, [1,2] )
                if axis == 5:
                    sample = flipSample( sample, [0,2] )
                if axis == 6:
                    sample = flipSample( sample, [0,1,2] )
                # if axis == 7 do nothing, keep sample as is.
            return sample
        except KeyboardInterrupt as err:    # Keyboard interrupt? Pass on up
            raise( err )
        except Exception as err:            # Other error? Try loading another sample
            #return self.nullSample

            if not self.silent:
                print(traceback.format_exc())
                print(err)
            if self.retryOnLoadingFailure:
                newIndex = random.randint( 0, len( self )-1 )
                if not self.silent:
                    print( "Warning: Error loading sample {:d}. Will try loading sample ({:d}).".format( i, newIndex ) )
                return self[newIndex]

    def loadSample( self, path, i, gridSize ):
        global printedNoMetaDataWarning

        # The input file
        filenameInput = os.path.join( path, "{:05d}".format(i), "voxelized.vts" )
        samplePath = os.path.join( path, "{:05d}".format(i) )

        if not os.path.isfile(filenameInput):
            raise IOError("Could not find file {}".format(filenameInput))

        # -------------------------------------------
        # Read input:
        reader = vtk.vtkXMLStructuredGridReader()
        reader.SetFileName(filenameInput)
        reader.Update()
        voxelsInput = reader.GetOutput()

        data = voxelsInput.GetPointData()
        preoperativeRaw = numpy_support.vtk_to_numpy( data.GetArray("preoperativeSurface") )
        intraoperativeRaw = numpy_support.vtk_to_numpy( data.GetArray("intraoperativeSurface") )

        intraoperative = numpy.reshape( intraoperativeRaw, (gridSize, gridSize, gridSize, 1) )
        intraoperative = numpy.transpose(intraoperative, (3,0,1,2) )
        preoperative = numpy.reshape( preoperativeRaw, (gridSize, gridSize, gridSize, 1) )
        preoperative = numpy.transpose(preoperative, (3,0,1,2) )

        mask = (preoperative < 0)

        # If no values in the SDF are lower than 0 then this is not a valid mesh.
        if not mask.any():
            raise IOError("Sample {} contains no internal points (no valid signed distance function?)".format(i))

        # If the displacement is known (training or test data), load it:
        displacement = None
        if data.HasArray("displacement"):
            displacementNonRigidRaw = numpy_support.vtk_to_numpy( data.GetArray("displacement") )
            displacementNonRigid = numpy.reshape( displacementNonRigidRaw, (gridSize, gridSize, gridSize, 3) )
            displacementNonRigid = numpy.transpose( displacementNonRigid, (3,0,1,2) )
            # If there is also a known rigid displacement, add it to the displacement:
            if data.HasArray("rigidDisplacement"):
                displacementRigidRaw = numpy_support.vtk_to_numpy( data.GetArray("rigidDisplacement") )
                displacementRigid = numpy.reshape( displacementRigidRaw, (gridSize, gridSize, gridSize, 3) )
                displacementRigid = numpy.transpose( displacementRigid, (3,0,1,2) )
                displacement = (displacementRigid + displacementNonRigid)*mask      # Add displacements
            else:
                displacement = displacementNonRigid*mask

            displacement = displacement*mask

        # -------------------------------------------
        # Put together training sample:
        sample = {}
        sample["path"] = filenameInput
        sample["index"] = i
        sample["preoperative"] = torch.FloatTensor(preoperative)
        sample["intraoperative"] = torch.FloatTensor(intraoperative)
        sample["visibleAmount"] = -1
        sample["displacementMax"] = -1
        if displacement is not None:
            sample["targetDisplacement"] = torch.FloatTensor(displacement)
        try:
            sample["visibleAmount"] = voxelsInput.GetFieldData().GetArray("visibleSurfaceAmount").GetTuple(0)[0]
        except Exception as e:
            if not printedNoMetaDataWarning:
                print("\r\033[KWarning: Could not load meta data (amount of visible surface)")
                printedNoMetaDataWarning = True
        try:
            sample["displacementMax"] = voxelsInput.GetFieldData().GetArray("displacement_max").GetTuple(0)[0]
        except Exception as e:
            if not printedNoMetaDataWarning:
                print("\r\033[KWarning: Could not load meta data (maximum displacement)")
                printedNoMetaDataWarning = True

        if sample["displacementMax"] > self.maxAllowedDisplacement:
            raise ValueError("Displacement over allowed threshold, ignoring sample")
        if sample["visibleAmount"] > -1 and sample["visibleAmount"] < self.minAllowedVisSurface:
            raise ValueError("Visible surface amount under allowed threshold, ignoring sample")

        return sample

        #except KeyboardInterrupt as err:    # If a keyboard interrupt was caught, pass it on
        #    print("KeyboardInterrupt")
        #    raise( err )
        #except Exception as err:
        #    print("\n",traceback.format_exc())
        #    print(err)
        #    return None

    def createNullSample( self ):
        sample = {}
        sample["path"] = "tmp.vts"
        sample["index"] = -1
        sample["preoperative"] = torch.zeros( 1, 64, 64, 64 )
        sample["intraoperative"] = torch.zeros( 1, 64, 64, 64 )
        sample["visibleAmount"] = -1
        sample["displacementMax"] = -1
        sample["targetDisplacement"] = torch.zeros( 3, 64, 64, 64 )

        self.nullSample = sample



############################################################
## Subset dataset
## Loads only the subset of a full dataset where the visible amount is above a certain threshold

class SubsetDataset( Dataset ):

    def __init__( self, fullDataset, minimumVisibleAmount ):
        self.fullDataset = fullDataset

        # Loop through the full dataset and check for which data the visible amount is greater than
        # the threshold. Remember the samples which fullfil this requirement.
        self.validSamples = []
        self.minimumVisibleAmount = minimumVisibleAmount
        for i in range( len(fullDataset) ):
            if fullDataset.visibleAmounts[i] >= minimumVisibleAmount:
                self.validSamples.append( i )

    def __len__( self ):
        return len( self.validSamples )

    def __getitem__( self, i ):
        return self.fullDataset[ self.validSamples[i] ]

############################################################
## Dataset tests

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description="Test loading of data")
    parser.add_argument("folder", type=path, help="Folder in which the samples lie")
    parser.add_argument("--num", type=int, default=10000, help="Number of samples to parse")
    parser.add_argument("--startnum", type=int, default=0, help="Number of first sample to parse")
    parser.add_argument("--flipTest", action="store_true", default=0, help="Test flipping")
    parser.add_argument("--sort", action="store_true", default=0, help="Sort by visiible amount")
    args = parser.parse_args()

    dataset = VoxelizedDataset( args.folder, False, args.startnum, args.num )
    print("Found {:d} valid samples.".format(len(dataset)))

    print("Pruning test:")
    dataset.setMaximumAllowedDisplacement( 0.2 )
    dataset.setMinimumAllowedVisSurface( 0.1 )
    dataset.prune()
    print("Remaining: {:d} valid samples.".format(len(dataset)))


    #subsetDataset = SubsetDataset( dataset, 0.9 )
    #print("Found {:d} valid samples for which visibleAmount >= 0.9.".format(len(subsetDataset)))

    #subsetDataset = SubsetDataset( dataset, 0.5 )
    #print("Found {:d} valid samples for which visibleAmount >= 0.5.".format(len(subsetDataset)))

    #subsetDataset = SubsetDataset( dataset, 0.1 )
    #print("Found {:d} valid samples for which visibleAmount >= 0.1.".format(len(subsetDataset)))
    
    if args.flipTest:
        import copy
        sample = dataset[0]
        for axis in range(0,8):
            s = copy.deepcopy(sample)
            if axis == 0:
                s = flipSample( s, [0] )
            if axis == 1:
                s = flipSample( s, [1] )
            if axis == 2:
                s = flipSample( s, [2] )
            if axis == 3:
                s = flipSample( s, [0,1] )
            if axis == 4:
                s = flipSample( s, [1,2] )
            if axis == 5:
                s = flipSample( s, [0,2] )
            if axis == 6:
                s = flipSample( s, [0,1,2] )
            saveSample( s["path"], "flipTest/{}.vts".format(axis), s )

    if args.sort:
        for i in range( len(dataset) ):
            print(i)
            #try:
            sample = dataset[i]
            print(sample["path"])

            if i/len(dataset) < 0.9:
                newFolder = os.path.dirname( os.path.dirname( sample["path"] ) )
                print(newFolder)
            #except:
                #pass


